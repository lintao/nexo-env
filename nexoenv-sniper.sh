#!/bin/bash
function nexoenv-sniper-name {
    echo sniper
}

function nexoenv-sniper-version {
    local msg="==== $FUNCNAME"
    version=$1
    nexo-svn-check-branches-name $version
    return $?
}

function nexoenv-sniper-url {
    local msg="==== $FUNCNAME: "
    local version=${1:-trunk}
    local branchesname=$(nexoenv-sniper-version $version)
    svnurl=$(nexo-svn-top)/$(nexoenv-sniper-name)/${branchesname}
    nexo-svn-check-repo-url $svnurl
    return $?
}

function nexoenv-sniper-revision-gen {
    nexo-svn-revision-gen $*
}

function nexoenv-sniper-checkout {
    # checkout all code
    local msg="==== $FUNCNAME: "
    local svnurl=$1
    local dstname=$2
    local revision=$3
    echo $msg checkout the code $svnurl 
    # check
    local flag=$(nexoenv-sniper-revision-gen $revision)
    echo $msg svn co $flag $svnurl $dstname 
    svn co $flag $svnurl $dstname 
}

function nexoenv-sniper-check-preq {
    local msg="==== $FUNCNAME: "
    echo $msg Pre Requirement Check
    source $NEXOENVDIR/nexoenv-env.sh
    local bashrcabspath=$(nexo-top-dir)/$(nexoenv-env-bashrc-name)
    if [ -f "$bashrcabspath" ]; then
        echo $msg source $bashrcabspath
        source $bashrcabspath
    else
        echo $msg Please using "nexoenv env" to setup the environment first
        exit 1
    fi
}
function nexoenv-sniper-compile {
    local msg="==== $FUNCNAME: "
    pushd $(nexo-top-dir) >& /dev/null
    if [ -d "$(nexoenv-sniper-name)" ]; then
        pushd $(nexoenv-sniper-name)/SniperRelease/cmt >& /dev/null
        cmt br cmt config
        source setup.sh
        cmt br cmt make
        popd >& /dev/null
    else
        echo $msg $(nexoenv-sniper-name) does not exist.
        echo $msg It seems the checkout failed.
    fi
    popd >& /dev/null
}

function nexoenv-sniper {
    local msg="=== $FUNCNAME: "
    # the main handler in this script
    local branchname=${1:-trunk}
    local revision=${2:-}
    # check version
    nexoenv-sniper-version $branchname
    if [ "$?" != "0" ]; then
        echo $msg branchesname ret: $?
        return 1
    fi


    local url=$(nexoenv-sniper-url $branchname) 
    echo $msg $?
    echo $msg URL: $url
    # change directory to $NEXOTOP
    pushd $(nexo-top-dir) >& /dev/null
    nexoenv-sniper-checkout $url $(nexoenv-sniper-name) $revision
    nexoenv-sniper-check-preq
    nexoenv-sniper-compile
    popd >& /dev/null

}
