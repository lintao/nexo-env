#!/bin/bash

# meta data

function nexoenv-EI-name {
    echo cmtlibs
}

function nexoenv-EI-top-name {
    echo ExternalInterface
}

function nexoenv-EI-version {
    local msg="==== $FUNCNAME"
    version=$1
    nexo-svn-check-branches-name $version
    return $?
}

function nexoenv-EI-url {
    local msg="==== $FUNCNAME: "
    local version=${1:-trunk}
    local branchesname=$(nexoenv-EI-version $version)
    svnurl=$(nexo-svn-top)/$(nexoenv-EI-name)/${branchesname}
    nexo-svn-check-repo-url $svnurl
    return $?
}

# checkout 
function nexoenv-EI-revision-gen {
    nexo-svn-revision-gen $*
}

function nexoenv-EI-checkout {
    # checkout all code
    local msg="==== $FUNCNAME: "
    local svnurl=$1
    local dstname=$2
    local revision=$3
    echo $msg checkout the code $svnurl 
    # check
    local flag=$(nexoenv-EI-revision-gen $revision)
    echo $msg svn co $flag $svnurl $dstname 
    svn co $flag $svnurl $dstname 
}

# check installation of run first
function nexoenv-EI-check-preq {
    # only check cmt is installed
    local msg="==== $FUNCNAME: "
    local env_scripts_dir=$NEXOENVDIR/packages
    source $NEXOENVDIR/nexoenv-external-libs.sh
    local deppkgs="cmt $(nexoenv-external-libs-list cmtlibs)"
    local guesspkg=""
    for guesspkg in $deppkgs
    do
        echo $msg setup ${env_scripts_dir}/${guesspkg}.sh
        source ${env_scripts_dir}/${guesspkg}.sh

        # check the bashrc and tcshrc in the External Libraries
        local installdir=$(nexo-ext-libs-${guesspkg}-install-dir)
        if [ -f "$installdir/bashrc" -a -f "$installdir/tcshrc" ]; then
            echo $msg setup $guesspkg
            source $installdir/bashrc
            echo $msg check again $guesspkg 
            which cmt || exit 1
        else
            echo $msg setup $guesspkg failed.
            echo Installation need terminate.
            exit 1
        fi
    done
    
}
# compile
function nexoenv-EI-compile {
    local msg="==== $FUNCNAME: "
    pushd $(nexo-top-dir) >& /dev/null
    if [ -f "$(nexoenv-env-bashrc-name)" ]; then
        source $(nexoenv-env-bashrc-name)
    else
        echo $msg Please use 'nexoenv env' to generate runtime setup files. 1>&2
        exit 1
    fi
    if [ -d "$(nexoenv-EI-top-name)" ]; then
        pushd $(nexoenv-EI-top-name) >& /dev/null
        pushd EIRelease/cmt
        cmt br cmt config
        popd
        popd >& /dev/null

    else
        echo $msg $(nexoenv-EI-top-name) does not exist. 1>&2
        echo $msg It seems the checkout failed. 1>&2
        exit 1
    fi
    popd >& /dev/null
}

# interface
# EI = External Interface
function nexoenv-external-interface {
    local msg="=== $FUNCNAME: "
    # the main handler in this script
    local branchname=${1:-trunk}
    local revision=${2:-}
    # check version
    nexoenv-EI-version $branchname
    if [ "$?" != "0" ]; then
        echo $msg branchesname ret: $?
        return 1
    fi

    local url=$(nexoenv-EI-url $branchname) 
    echo $msg $?
    echo $msg URL: $url@$revision
    # change directory to $NEXOTOP
    pushd $NEXOTOP >& /dev/null
    nexoenv-EI-checkout $url $(nexoenv-EI-top-name) $revision
    # TODO
    # check the installation
    nexoenv-EI-check-preq
    nexoenv-EI-compile
    popd
}
