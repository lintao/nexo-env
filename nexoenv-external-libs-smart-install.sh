#!/bin/bash

function nexo-ext-libs-smart-install() {
    local msg="==== $FUNCNAME: "
    local pkg=$1; shift
    local ver=$1; shift

    echo $msg smart install $pkg $ver
    nexo-ext-libs-all $pkg
}
