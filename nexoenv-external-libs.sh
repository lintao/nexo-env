#!/bin/bash

VALID_SUB_COMMAND="all get conf make install setup reuse"

function nexo-ext-libs-gen-cpu-num {
    local cpus=$(grep -c ^processor /proc/cpuinfo)
    case cpus in
        1)
            echo ""
            ;;
        *)
            echo -j${cpus}
            ;;
    esac
}

function nexo-ext-libs-check-sub-command {
    local msg="==== $FUNCNAME: "
    cmd=$1
    case $cmd in
        smart-install)
            return 0
            ;;
        all)
            return 0
            ;;
        get)
            return 0
            ;;
        conf)
            return 0
            ;;
        make)
            return 0
            ;;
        install)
            return 0
            ;;
        setup)
            return 0
            ;;
        reuse)
            return 0
            ;;
        *)
            echo $msg unknown sub command $cmd
            return 1
            ;;
    esac

}

function nexo-ext-libs-generate-sh {
local pkg=$1
local install=$2
local lib=${3:-lib}
cat << EOF > bashrc
export NEXO_EXTLIB_${pkg}_HOME=${install}
export PATH=\${NEXO_EXTLIB_${pkg}_HOME}/bin:\${PATH}
EOF

    # check the lib or lib64
    for lib in lib lib64
    do
cat << EOF >> bashrc
if [ -d \${NEXO_EXTLIB_${pkg}_HOME}/${lib} ];
then
export LD_LIBRARY_PATH=\${NEXO_EXTLIB_${pkg}_HOME}/${lib}:\${LD_LIBRARY_PATH}
fi
if [ -d \${NEXO_EXTLIB_${pkg}_HOME}/${lib}/pkgconfig ];
then
export PKG_CONFIG_PATH=\${NEXO_EXTLIB_${pkg}_HOME}/${lib}/pkgconfig:\${PKG_CONFIG_PATH}
fi
EOF
    done

cat << EOF >> bashrc
export CPATH=\${NEXO_EXTLIB_${pkg}_HOME}/include:\${CPATH}
export MANPATH=\${NEXO_EXTLIB_${pkg}_HOME}/share/man:\${MANPATH}
EOF
    # user defined generate
    type -t nexo-ext-libs-${curpkg}-generate-sh >& /dev/null
    if [ "$?" = 0 ]; then
        echo $msg call nexo-ext-libs-${curpkg}-generate-sh to generate user defined 
        nexo-ext-libs-${curpkg}-generate-sh $@
    fi
}

function nexo-ext-libs-generate-csh {
    local msg="==== $FUNCNAME: "
local pkg=$1
local install=$2
local lib=${3:-lib}
cat << EOF > tcshrc
if ( \$?PATH == 0 ) then
    setenv PATH ""
endif
if ( \$?LD_LIBRARY_PATH == 0 ) then
    setenv LD_LIBRARY_PATH ""
endif
if ( \$?PKG_CONFIG_PATH == 0 ) then
    setenv PKG_CONFIG_PATH ""
endif
if ( \$?CPATH == 0 ) then
    setenv CPATH ""
endif
if ( \$?MANPATH == 0 ) then
    setenv MANPATH ""
endif
setenv NEXO_EXTLIB_${pkg}_HOME ${install}
setenv PATH \${NEXO_EXTLIB_${pkg}_HOME}/bin:\${PATH}
EOF

    # check the lib or lib64
    for lib in lib lib64
    do
cat << EOF >> tcshrc
if ( -d \${NEXO_EXTLIB_${pkg}_HOME}/${lib}) then
setenv LD_LIBRARY_PATH \${NEXO_EXTLIB_${pkg}_HOME}/${lib}:\${LD_LIBRARY_PATH}
endif
if ( -d \${NEXO_EXTLIB_${pkg}_HOME}/${lib}/pkgconfig) then
setenv PKG_CONFIG_PATH \${NEXO_EXTLIB_${pkg}_HOME}/${lib}/pkgconfig:\${PKG_CONFIG_PATH}
endif
EOF
cat << EOF >> tcshrc
setenv CPATH \${NEXO_EXTLIB_${pkg}_HOME}/include:\${CPATH}
setenv MANPATH \${NEXO_EXTLIB_${pkg}_HOME}/share/man:\${MANPATH}
EOF
    done

    # user defined generate
    type -t nexo-ext-libs-${curpkg}-generate-csh >& /dev/null
    if [ "$?" = 0 ]; then
        echo $msg call nexo-ext-libs-${curpkg}-generate-csh to generate user defined 
        nexo-ext-libs-${curpkg}-generate-csh $@
    fi

}

function nexo-ext-libs-check-init {
    local msg="==== $FUNCNAME: "
    # check the bootstrap script of pkg 
    local pkg=$1
    if [ -f "$(nexo-ext-libs-init-scripts-dir)/$pkg.sh" ]; then
        source $(nexo-ext-libs-init-scripts-dir)/$pkg.sh 
        # setup the dependencies
        type -t nexo-ext-libs-${pkg}-dependencies-setup >& /dev/null
        if [ "$?" = "0" ]; then
            echo $msg setup dependencies for $pkg
            nexo-ext-libs-${pkg}-dependencies-setup
        else
            echo $msg no dependencies for $pkg
        fi
        return 0
    else
        echo $msg $(nexo-ext-libs-init-scripts-dir)/$pkg.sh does not exist 1>&2 
        echo $msg If you want to install $pkg, please add the bootstrap script first. 1>&2
        return 1
    fi
}

function nexo-ext-libs-check-is-reused {
    local msg="==== $FUNCNAME: "
    # just check the install prefix is a soft link or not
    local pkg=$1
    local newpath=$(nexo-ext-libs-${pkg}-install-dir)
    if [[ -L "$newpath" && -d "$newpath" ]];
    then
        echo $msg The installation prefix for $pkg: \"$newpath\" is a soft link. 1>&2
        echo $msg It can be a reused library. 1>&2
        return 1
    else
        return 0
    fi
}

# create a function to override the default version
function nexo-ext-libs-create-PKG-version {
    local msg="==== $FUNCNAME: "
    local pkg=$1
    local ver=$2
    # make sure $ver is not null
    if [ -z "$ver" ]; then
        echo $msg $pkg version is null. 1>&2
        echo $msg will not create nexo-ext-libs-${pkg}-version- 1>&2
        return 1
    fi
    # create function
    eval "nexo-ext-libs-${pkg}-version-(){ echo "$ver";}"

    echo $msg Create Function nexo-ext-libs-${pkg}-version-
    echo $msg Call nexo-ext-libs-${pkg}-version-: $(nexo-ext-libs-${pkg}-version-)
}

function nexo-ext-libs-PKG-version {
    local curpkg=$1
    # check override
    type -t nexo-ext-libs-${curpkg}-version- >& /dev/null
    if [ "$?" = "0" ]; then
        # user defined 
        echo $(nexo-ext-libs-${curpkg}-version-)
    else
        echo $(nexo-ext-libs-${curpkg}-version-default)
    fi
}

function nexo-ext-libs-file-check-exist {
    # if exists, return 0
    # else, return 1
    fn="$1"
    if [ -f "$fn" ]; then
        return 0
    else
        return 1
    fi
}

function nexo-ext-libs-init-scripts-dir {
    echo "$NEXOENVDIR/packages"
}

function nexo-ext-libs-install-root {
    echo "$NEXOTOP/ExternalLibs"
}

function nexo-ext-libs-install-root-check {
    local msg="==== $FUNCNAME: "
    if [ ! -d "$(nexo-ext-libs-install-root)" ]; then
        mkdir -p $(nexo-ext-libs-install-root)
    fi
    if [ -d "$(nexo-ext-libs-install-root)" ]; then
        return 0
    else
        echo $msg Create $(nexo-ext-libs-install-root) Failed.
        return 1
    fi
}

function nexo-ext-libs-build-root {
    echo "$NEXOTOP/ExternalLibs/Build"
}

function nexo-ext-libs-build-root-check {
    local msg="==== $FUNCNAME: "
    if [ ! -d "$(nexo-ext-libs-build-root)" ]; then
        mkdir -p $(nexo-ext-libs-build-root)
    fi
    if [ -d "$(nexo-ext-libs-build-root)" ]; then
        return 0
    else
        echo $msg Create $(nexo-ext-libs-build-root) Failed.
        return 1
    fi
}

# dependencies setup (common structure)
#############################################################################
# new implementation using recursive 
#############################################################################
function nexo-ext-libs-dependencies-setup-setup-one-package-init() {
    # this is used to load the dependencies list
    local deppkg=$1; shift;
    local depver=$1; shift;

    # create log directory for pkg
    if [ ! -d "$(nexo-ext-libs-log-dir $deppkg)" ]; then
        echo $msg create log directory $(nexo-ext-libs-log-dir $deppkg)
        mkdir -p $(nexo-ext-libs-log-dir $deppkg)
        # check again
        [ ! -d "$(nexo-ext-libs-log-dir $deppkg)" ] && exit -1
    fi

    # create version function
    echo $msg create function nexo-ext-libs-${deppkg}-version- to override default
    if [ -n "$depver" ]; then
        nexo-ext-libs-create-PKG-version $deppkg $depver
    fi 
    # init script
    # only after setup the init script, we can get the list of the dependencies.
    if [ -f "$(nexo-ext-libs-init-scripts-dir)/${deppkg}.sh" ]; then
        echo $msg source $(nexo-ext-libs-init-scripts-dir)/${deppkg}.sh
        source $(nexo-ext-libs-init-scripts-dir)/${deppkg}.sh
        echo $msg After source: ${deppkg} ${depver}
    else
        echo $msg can not find $deppkg.sh 1>&2
        exit 1
    fi
}
function nexo-ext-libs-dependencies-setup-setup-one-package-runtime-env() {
    # this is only called after the dependencies are source
    local optional=${1:-0}; shift
    local deppkg=$1; shift
    local depver=$1; shift
    # setup script
    if [ -f "$(nexo-ext-libs-${deppkg}-install-dir)/bashrc" ]; then
        echo $msg source $(nexo-ext-libs-${deppkg}-install-dir)/bashrc
        source $(nexo-ext-libs-${deppkg}-install-dir)/bashrc
    elif [ "$optional" == "1" ]; then
        echo $msg $deppkg is not installed by nexoenv, but it is optional.
        echo $msg SKIP setup $deppkg
    else
        # check smart mode
        nexo-ext-libs-smart-install-mode || {
            # not smart mode, so just exit the whole program
            echo $msg can not find $(nexo-ext-libs-${deppkg}-install-dir)/bashrc 1>&2
            echo $msg Please install $deppkg first. 1>&2
            exit 1
        }
        nexo-ext-libs-smart-install $deppkg $depver
    fi
}
function nexo-ext-libs-dependencies-setup-rec-impl {
    local _level=$1; shift
    local curpkg=$1; shift # this is the pkg to be intalled.
    local skipfn=$1;
    # remove the optional 
    local deppkgverarr=($(echo $curpkg | tr "@" "\n"))
    local deppkg=${deppkgverarr[0]}
    local depver=${deppkgverarr[1]}
    local optional=0
    case $deppkg in
        +*)
            optional=1
            deppkg=${deppkg:1}
            ;;
    esac
    local msg="==== $FUNCNAME: ${_level} setup $deppkg:"
    # check whether the package is setup already or not.
    # if the package is already setup, skip it
    # XXX: if the package is the current package (flag=skip), we need to skip this flag,
    #      else it will don't setup later.
    if [ -z "$skipfn" ]; then
        if type -t nexo-ext-libs-${deppkg}-status-already-setup >& /dev/null; then
            echo $msg $deppkg already setup 1>&2
            return;
        else
            # create this function
            eval "nexo-ext-libs-${deppkg}-status-already-setup(){ echo "1";}"
        fi
    fi
    # load the dependencies function
    nexo-ext-libs-dependencies-setup-setup-one-package-init $deppkg $depver
    # handle all dependencies
    local deppkgver
    if type -t nexo-ext-libs-${deppkg}-dependencies-list >& /dev/null ; then
        for deppkgver in $(nexo-ext-libs-${deppkg}-dependencies-list)
        do
            nexo-ext-libs-dependencies-setup-rec-impl "${_level}#" $deppkgver
        done
    fi
    # setup the runtime env
    # but if $skipfn is true, skip the following function
    if [ -z "$skipfn" ]; then
        echo $msg status: $optional $deppkg $depver 
        nexo-ext-libs-dependencies-setup-setup-one-package-runtime-env $optional $deppkg $depver 
    fi
}
function nexo-ext-libs-dependencies-setup {
    local curpkg=$1 # this is the pkg to be intalled.
    local _level="#"
    nexo-ext-libs-dependencies-setup-rec-impl ${_level} $curpkg skip
}
#############################################################################
function nexo-ext-libs-dependencies-setup-old {
    local curpkg=$1 # this is the pkg to be intalled.
    local msg="==== $FUNCNAME: setup $curpkg:"
    local deppkgver=""
    echo $msg Dependencies: $(nexo-ext-libs-${curpkg}-dependencies-list)

    for deppkgver in $(nexo-ext-libs-${curpkg}-dependencies-list)
    do
        echo $msg Handle Dependency: $deppkgver
        local deppkgverarr=($(echo $deppkgver | tr "@" "\n"))
        local deppkg=${deppkgverarr[0]}
        local depver=${deppkgverarr[1]}

        local optional=0
        # if start with '+', it means optional
        case $deppkg in
            +*)
                optional=1
                deppkg=${deppkg:1}
                ;;
        esac
        # create version function
        echo $msg create function nexo-ext-libs-${deppkg}-version- to override default
        if [ -n "$depver" ]; then
            nexo-ext-libs-create-PKG-version $deppkg $depver
        fi 
        # init script
        if [ -f "$(nexo-ext-libs-init-scripts-dir)/${deppkg}.sh" ]; then
            echo $msg source $(nexo-ext-libs-init-scripts-dir)/${deppkg}.sh
            source $(nexo-ext-libs-init-scripts-dir)/${deppkg}.sh
            echo $msg After source: ${deppkg} ${depver}
        else
            echo $msg can not find $deppkg.sh
            exit 1
        fi
        # setup script
        if [ -f "$(nexo-ext-libs-${deppkg}-install-dir)/bashrc" ]; then
            echo $msg source $(nexo-ext-libs-${deppkg}-install-dir)/bashrc
            source $(nexo-ext-libs-${deppkg}-install-dir)/bashrc
        elif [ "$optional" == "1" ]; then
            echo $msg $deppkg is not installed by nexoenv, but it is optional.
            echo $msg SKIP setup $deppkg
        else
            echo $msg can not find $(nexo-ext-libs-${deppkg}-install-dir)/bashrc
            echo $msg Please install $deppkg first.
            exit 1
        fi
    done
}
#############################################################################

# helper for get
function nexo-ext-libs-PKG-get {
    local curpkg=$1 # this is the pkg to be intalled.
    shift
    local msg="===== $FUNCNAME: "
    local version=${1:-$(nexo-ext-libs-${curpkg}-version)}
    local downloadurl=$(nexo-ext-libs-${curpkg}-download-url $version)

    nexo-ext-libs-build-root-check || exit $?
    pushd $(nexo-ext-libs-build-root) >& /dev/null
        tarname=$(nexo-ext-libs-${curpkg}-download-filename)
        nexo-ext-libs-${curpkg}-file-check-exist $tarname 
        # already exists
        if [ "$?" = "0" ]; then
            echo $msg SKIP DOWNLOADING: $tarname already exists
        else
            echo $msg Download
            # TODO
            # a common tool to download
            if [ "$(nexo-archive-check)" == "0" ]; then 
                wget -O $tarname $downloadurl || exit $?
            else
                nexo-archive-get $tarname $tarname || exit $?
            fi
        fi
    popd >& /dev/null
}

# helper for conf
function nexo-ext-libs-PKG-conf-uncompress() {
    local msg="===== $FUNCNAME: "
    local tardst=$1
    local tarname=$2

    case $tarname in
        *.tar.gz|*.tgz)
            echo $msg tar zxvf $tarname 
            tar zxvf $tarname || exit $?
            ;;
        *.zip)
            echo $msg unzip $tarname 
            unzip $tarname || exit $?
            ;;
        *.tar.bz2)
            echo $msg tar jxvf $tarname 
            tar jxvf $tarname || exit $?
            ;;
        *)
            echo $msg Can not uncompress the file $tarname 1>&2
            exit 1
            ;;
    esac

    if [ ! -d "$tardst" ]; then
        echo $msg "After Uncompress, can't find $tardst"
        exit 1
    fi
}
function nexo-ext-libs-PKG-conf {
    local curpkg=$1 # this is the pkg to be intalled.
    shift
    local msg="===== $FUNCNAME: "
    nexo-ext-libs-build-root-check || exit $?
    pushd $(nexo-ext-libs-build-root) >& /dev/null

    local tarname=$(nexo-ext-libs-${curpkg}-download-filename)
    nexo-ext-libs-${curpkg}-file-check-exist $tarname
    # does not exist, so download
    if [ "$?" != "0" ]; then
        nexo-ext-libs-${curpkg}-get
    fi
    # check again, if still not exist, exit
    nexo-ext-libs-${curpkg}-file-check-exist $tarname || exit $?
    # unzip the file if the directory does not exist
    tardst=$(nexo-ext-libs-${curpkg}-tardst)
    if [ ! -d "$tardst" ]; then
        echo $msg Uncompress the $tarname
        if type -t nexo-ext-libs-${curpkg}-conf-uncompress >& /dev/null;
        then
            nexo-ext-libs-${curpkg}-conf-uncompress $tardst $tarname
        else
            nexo-ext-libs-PKG-conf-uncompress $tardst $tarname
        fi
    fi
    pushd $tardst >& /dev/null
    
    if [ ! -d "$(nexo-ext-libs-${curpkg}-install-dir)" ]; then
        mkdir -p $(nexo-ext-libs-${curpkg}-install-dir)
    fi

    nexo-ext-libs-${curpkg}-conf- 

    popd >& /dev/null
    popd >& /dev/null

}

# helper for make
function nexo-ext-libs-PKG-make {
    local curpkg=$1 # this is the pkg to be intalled.
    shift
    local msg="===== $FUNCNAME: "
    nexo-ext-libs-build-root-check || exit $?
    pushd $(nexo-ext-libs-build-root) >& /dev/null

    local tardst=$(nexo-ext-libs-${curpkg}-tardst)
    pushd $tardst >& /dev/null
    if [ "$?" != "0" ]; then
        echo $msg Please configure the package first.
        exit 1
    fi

    # check the function exist
    type -t nexo-ext-libs-${curpkg}-make- >& /dev/null
    if [ "$?" = "0" ]; then
        echo $msg call nexo-ext-libs-${curpkg}-make-
        nexo-ext-libs-${curpkg}-make-
    else
        echo $msg call default 
        echo $msg make $(nexo-ext-libs-gen-cpu-num)
        make $(nexo-ext-libs-gen-cpu-num) || exit $?
    fi

    popd >& /dev/null
    popd >& /dev/null
}

# helper for install
function nexo-ext-libs-PKG-install {
    local curpkg=$1 # this is the pkg to be intalled.
    shift
    local msg="===== $FUNCNAME: "
    nexo-ext-libs-build-root-check || exit $?
    pushd $(nexo-ext-libs-build-root) >& /dev/null
    
    local tardst=$(nexo-ext-libs-${curpkg}-tardst)
    pushd $tardst >& /dev/null
    if [ "$?" != "0" ]; then
        echo $msg Please configure the package first.
        exit 1
    fi

    # check the function exist
    type -t nexo-ext-libs-${curpkg}-install- >& /dev/null
    if [ "$?" = "0" ]; then
        echo $msg call nexo-ext-libs-${curpkg}-install-
        nexo-ext-libs-${curpkg}-install-
    else
        echo $msg call default 
        echo $msg make install
        make install || exit $?
    fi
    
    popd >& /dev/null
    popd >& /dev/null

}


# helper for setup
function nexo-ext-libs-PKG-setup {
    local curpkg=$1 # this is the pkg to be intalled.
    shift
    local msg="===== $FUNCNAME: "
    nexo-ext-libs-install-root-check || exit $?
    pushd $(nexo-ext-libs-install-root) >& /dev/null

    if [ ! -d "$(nexo-ext-libs-${curpkg}-install-dir)" ]; then
        echo $msg Please install the Package first
        exit 1
    fi
    local install=$(nexo-ext-libs-${curpkg}-install-dir)
    pushd $install
    nexo-ext-libs-generate-sh $(nexo-ext-libs-${curpkg}-name) ${install}
    nexo-ext-libs-generate-csh $(nexo-ext-libs-${curpkg}-name) ${install}
    popd
    
    popd >& /dev/null
}

function nexo-ext-libs-log-dir() {
    local pkg=$1; shift
    echo $NEXOENVDIR/logs/${pkg}
}

function nexo-ext-libs-log-file() {
    local pkg=$1; shift
    local cmd=$1; shift
    echo $(nexo-ext-libs-log-dir ${pkg})/${pkg}-${cmd}.log
}
# interface

function nexo-ext-libs-get {
    local msg="==== $FUNCNAME: "
    local pkg=$1
    echo $msg
    nexo-ext-libs-$pkg-get | sed 's/^/['"${pkg}"'-conf] /' | tee $(nexo-ext-libs-log-file $pkg get)
}
function nexo-ext-libs-conf {
    local msg="==== $FUNCNAME: "
    local pkg=$1
    echo $msg
    nexo-ext-libs-$pkg-conf | sed 's/^/['"${pkg}"'-conf] /' | tee $(nexo-ext-libs-log-file $pkg conf)
}
function nexo-ext-libs-make {
    local msg="==== $FUNCNAME: "
    local pkg=$1
    echo $msg
    nexo-ext-libs-$pkg-make | sed 's/^/['"${pkg}"'-make] /' | tee $(nexo-ext-libs-log-file $pkg make)
}
function nexo-ext-libs-install {
    local msg="==== $FUNCNAME: "
    local pkg=$1
    echo $msg
    nexo-ext-libs-$pkg-install | sed 's/^/['"${pkg}"'-install] /' | tee $(nexo-ext-libs-log-file $pkg install)
}
function nexo-ext-libs-setup {
    local msg="==== $FUNCNAME: "
    local pkg=$1
    echo $msg
    nexo-ext-libs-$pkg-setup | sed 's/^/['"${pkg}"'-setup] /' | tee $(nexo-ext-libs-log-file $pkg setup)
}

function nexo-ext-libs-all {
    local msg="==== $FUNCNAME: "
    local pkg=$1
    nexo-ext-libs-get $pkg
    nexo-ext-libs-conf $pkg
    nexo-ext-libs-make $pkg
    nexo-ext-libs-install $pkg
    nexo-ext-libs-setup $pkg
}

function nexo-ext-libs-reuse {
    local msg="==== $FUNCNAME: "
    # = check the environment variable $NEXO_EXTLIB_OLDTOP =
    if [ -z "$NEXO_EXTLIB_OLDTOP" ];
    then
        echo $msg Please set the ENVIRONMENT VARIABLE called \$NEXO_EXTLIB_OLDTOP first1>&2
        exit 1
    fi
    if [ ! -d "$NEXO_EXTLIB_OLDTOP" ];
    then
        echo $msg The \$NEXO_EXTLIB_OLDTOP \"$NEXO_EXTLIB_OLDTOP\" does not exist.
        exit 1
    fi
    local pkg=$1
    # = get the installation directory for PKG =
    # here is the dir with version
    local oldpath=$NEXO_EXTLIB_OLDTOP/$(nexo-ext-libs-${pkg}-name)/$(nexo-ext-libs-${pkg}-version)
    local newpath=$(nexo-ext-libs-${pkg}-install-dir)
    echo $msg $pkg oldpath: $oldpath
    echo $msg $pkg newpath: $newpath
    local retst=0
    if [ ! -d "$oldpath" ];
    then
        echo $msg $pkg oldpath \"$oldpath\" does not exist. 1>&2
        retst=1
    fi
    if [ -d "$newpath" ];
    then
        echo $msg $pkg newpath \"$newpath\" already exists. 1>&2
        retst=1
    fi
    if [ "$retst" != "0" ];
    then
        return $retst
    fi
    # = prepare the external library path for the package =
    nexo-ext-libs-install-root-check || exit $?
    # == ExternalLibs ==
    pushd $(nexo-ext-libs-install-root) >& /dev/null
    # === PKG ===
    if [ ! -d $(nexo-ext-libs-$pkg-name) ];
    then
        mkdir $(nexo-ext-libs-$pkg-name)
    fi
    pushd $(nexo-ext-libs-$pkg-name) >& /dev/null
    # ==== create link here ===
    ln -s $oldpath $newpath
    popd >& /dev/null
    popd >& /dev/null
}

function nexoenv-external-libs-list { 
    local mode=$1 
    echo python boost cmake 
    if [ "$mode" == "cmtlibs" ] || [ "$mode" == "reuse" ]; 
    then 
        echo  
    else 
        echo git 
    fi
    echo gccxml xercesc 
    if [ "$mode" == "cmtlibs" ] || [ "$mode" == "reuse" ]; 
    then 
        echo  
    else 
        echo qt4
    fi 
    echo gsl
    echo cmt clhep ROOT hepmc geant4 
    echo libmore  
    if [ "$mode" == "cmtlibs" ] || [ "$mode" == "reuse" ]; 
    then 
        echo  
    else 
        echo libmore-data 
    fi 
} 

function nexoenv-external-libs {
    local msg="=== $FUNCNAME: "
    local cmd=$1
    nexo-ext-libs-check-sub-command $cmd || exit $?
    shift
    local packages=$@

    # smart install
    # note here:
    # if the mode is smart-install, create a function called 'nexo-ext-libs-smart-install-mode' return 0
    case $cmd in
        smart-install)
            eval "nexo-ext-libs-smart-install-mode(){ return 0;}"
            source nexoenv-external-libs-smart-install.sh
            ;;
        *)
            eval "nexo-ext-libs-smart-install-mode(){ return 1;}"
            ;;
    esac

    if [ "$packages" = "allpkgs" ]; 
    then 
        echo $msg allpkgs will be loaded 
        packages=$(nexoenv-external-libs-list $cmd) 
        echo $msg $packages 
    fi 

    # check dir first

    echo $msg command: $cmd
    echo $msg packages: $packages
    local pkgver=""
    for pkgver in $packages 
    do
        # using @ to seperate pkg anv ver.
        local pkgverarr=($(echo $pkgver | tr "@" "\n"))
        local pkg=${pkgverarr[0]}
        local ver=${pkgverarr[1]}
        # create version function first
        echo $msg create function nexo-ext-libs-${pkg}-version- to override default
        if [ -n "$ver" ]; then
            nexo-ext-libs-create-PKG-version $pkg $ver
        fi 
        # check the initial status
        echo $msg nexo-ext-libs-check-init $pkg
        nexo-ext-libs-check-init $pkg || exit $?
        # check whether the library is reused or not.
        echo $msg nexo-ext-libs-check-is-reused $pkg
        nexo-ext-libs-check-is-reused $pkg || continue
        echo $msg nexo-ext-libs-$cmd $pkg
        nexo-ext-libs-$cmd $pkg
    done

}
