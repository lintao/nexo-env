#!/bin/bash

# meta data
function nexo-ext-libs-ROOT-name {
    echo ROOT
}

function nexo-ext-libs-ROOT-version-default {
    #echo 5.34.11
    echo 5.34.36
}
function nexo-ext-libs-ROOT-version {
    echo $(nexo-ext-libs-PKG-version ROOT)
}

# dependencies
function nexo-ext-libs-ROOT-dependencies-list {
    echo python boost cmake +git gccxml xercesc +qt4 gsl
}
function nexo-ext-libs-ROOT-dependencies-setup {
    nexo-ext-libs-dependencies-setup ROOT
}
# install dir
function nexo-ext-libs-ROOT-install-dir {
    local version=${1:-$(nexo-ext-libs-ROOT-version)}
    echo $(nexo-ext-libs-install-root)/$(nexo-ext-libs-ROOT-name)/$version
}
# download info
function nexo-ext-libs-ROOT-download-url {
    local msg="===== $FUNCNAME: "
    local version=${1:-$(nexo-ext-libs-ROOT-version)}
    case $version in
        5.34.11)
            echo ftp://root.cern.ch/root/root_v5.34.11.source.tar.gz
            ;;
        5.34.*)
            echo http://root.cern.ch/download/root_v${version}.source.tar.gz
            ;;
        *)
            echo $msg Unknown version $version, using the default one 1>&2
            echo ftp://root.cern.ch/root/root_v5.34.11.source.tar.gz
            ;;
    esac
}
function nexo-ext-libs-ROOT-download-filename {
    local msg="===== $FUNCNAME: "
    local version=${1:-$(nexo-ext-libs-ROOT-version)}
    case $version in
        5.34.11)
            echo root_v5.34.11.source.tar.gz
            ;;
        5.34.*)
            echo root_v${version}.source.tar.gz
            ;;
        *)
            echo $msg Unknown version $version, using the default one 1>&2
            echo root_v5.34.11.source.tar.gz
            ;;
    esac
}

function nexo-ext-libs-ROOT-tardst {
    # install in root of external libraries
    local version=${1:-$(nexo-ext-libs-ROOT-version)}
    case $version in
        5.34.11)
            echo root-5.34.11
            ;;
        5.34.*)
            echo root-${version}
            ;;
        *)
            echo root-5.34.11
            ;;
    esac
}

function nexo-ext-libs-ROOT-file-check-exist {
    nexo-ext-libs-file-check-exist $@
    return $?
}

# interface
function nexo-ext-libs-ROOT-get {
    nexo-ext-libs-PKG-get ROOT
}

function nexo-ext-libs-ROOT-conf-uncompress {
    local msg="===== $FUNCNAME: "
    tardst=$1
    tarname=$2
    echo tar -C $tardst -zxvf $tarname
    if [ -d "$tardst" ]; then
        echo $msg $tardst already exist
        echo $msg will not invoke the uncompress command
        return
    else
        mkdir -p $tardst
    fi
    tar -C $tardst -zxvf $tarname || exit $?
    if [ ! -d "$tardst" ]; then
        echo $msg "After Uncompress, can't find $tardst"
        exit 1
    fi
}

function nexo-ext-libs-ROOT-conf-env-setup {
    local msg="===== $FUNCNAME: "
    # GSL
    if [ -d "$(nexo-ext-libs-gsl-install-dir)" ]; then
        export GSL=$(nexo-ext-libs-gsl-install-dir)
        echo $msg GSL=${GSL}
    fi
    # QTDIR
    if [ -d "$(nexo-ext-libs-qt4-install-dir)" ]; then
        export QTDIR=$(nexo-ext-libs-qt4-install-dir)
        echo $msg QTDIR=${QTDIR}
    fi
}

function nexo-ext-libs-ROOT-get-patch-name {
    echo $(nexo-ext-libs-init-scripts-dir)/patches/root-5.34.patch
}

function nexo-ext-libs-ROOT-apply-patch {
    local msg="===== $FUNCNAME: "
    # load patch file name
    local patchname=$(nexo-ext-libs-ROOT-get-patch-name)
    echo $msg Apply Patch $patchname
    # apply patch
    patch -p0 < $patchname
}

function nexo-ext-libs-ROOT-conf- {
    local msg="===== $FUNCNAME: "
    pushd root

    #apply patch
    #nexo-ext-libs-ROOT-apply-patch

    #echo $msg ./configure --prefix=$(nexo-ext-libs-ROOT-install-dir) --etcdir=$(nexo-ext-libs-ROOT-install-dir)/etc --all 
    #./configure --prefix=$(nexo-ext-libs-ROOT-install-dir) --etcdir=$(nexo-ext-libs-ROOT-install-dir)/etc --all

    export ROOTSYS=$(nexo-ext-libs-ROOT-install-dir)
    # note, root uses several external Environment Variables
    nexo-ext-libs-ROOT-conf-env-setup
    echo $msg ./configure  --all 
    ./configure --all --disable-xrootd
    popd
}
function nexo-ext-libs-ROOT-conf {
    nexo-ext-libs-PKG-conf ROOT
}
function nexo-ext-libs-ROOT-make- {
    local msg="===== $FUNCNAME: "
    pushd root
    export ROOTSYS=$(nexo-ext-libs-ROOT-install-dir)
    echo $msg make $(nexo-ext-libs-gen-cpu-num)
    make $(nexo-ext-libs-gen-cpu-num)
    popd
}
function nexo-ext-libs-ROOT-make {
    nexo-ext-libs-PKG-make ROOT
}
function nexo-ext-libs-ROOT-install- {
    local msg="===== $FUNCNAME: "
    pushd root
    export ROOTSYS=$(nexo-ext-libs-ROOT-install-dir)
    echo $msg make install
    make install
    popd
}

function nexo-ext-libs-ROOT-install {
    nexo-ext-libs-PKG-install ROOT
}

function nexo-ext-libs-ROOT-generate-sh {
local pkg=$1
local install=$2
local lib=${3:-lib}
cat << EOF >> bashrc
source \${NEXO_EXTLIB_${pkg}_HOME}/bin/thisroot.sh
EOF
}
function nexo-ext-libs-ROOT-generate-csh {
local pkg=$1
local install=$2
local lib=${3:-lib}
cat << EOF >> tcshrc
setenv ROOTSYS \${NEXO_EXTLIB_${pkg}_HOME}
setenv PATH \${NEXO_EXTLIB_${pkg}_HOME}/bin:\${PATH}
setenv LD_LIBRARY_PATH \${NEXO_EXTLIB_${pkg}_HOME}/${lib}:\${LD_LIBRARY_PATH}
setenv CPATH \${NEXO_EXTLIB_${pkg}_HOME}/include:\${CPATH}
if ( \$?PYTHONPATH == 0 ) then
    setenv PYTHONPATH ""
endif
setenv PYTHONPATH \${NEXO_EXTLIB_${pkg}_HOME}/${lib}:\${PYTHONPATH} 
EOF
}

function nexo-ext-libs-ROOT-setup {
    nexo-ext-libs-PKG-setup ROOT
}
