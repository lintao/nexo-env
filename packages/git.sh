#!/bin/bash

# meta data

function nexo-ext-libs-git-name {
    echo Git
}

function nexo-ext-libs-git-version-default {
    echo 1.8.4.3
}
function nexo-ext-libs-git-version {
    echo $(nexo-ext-libs-PKG-version git)
}

# dependencies
function nexo-ext-libs-git-dependencies-list {
    echo
}
function nexo-ext-libs-git-dependencies-setup {
    nexo-ext-libs-dependencies-setup git
}

# install dir
function nexo-ext-libs-git-install-dir {
    local version=${1:-$(nexo-ext-libs-git-version)}
    echo $(nexo-ext-libs-install-root)/$(nexo-ext-libs-git-name)/$version
}

# download info
function nexo-ext-libs-git-download-url {
    local version=${1:-$(nexo-ext-libs-git-version)}
    case $version in
        1.8.4.3)
            echo https://github.com/git/git/archive/v1.8.4.3.zip
            ;;
        *) 
            echo https://github.com/git/git/archive/v1.8.4.3.zip
            ;;
    esac
}

function nexo-ext-libs-git-download-filename {
    local version=${1:-$(nexo-ext-libs-git-version)}
    case $version in
        1.8.4.3)
            echo git-1.8.4.3.zip
            ;;
            *)
            echo git-1.8.4.3.zip
            ;;
    esac

}

# build info
function nexo-ext-libs-git-tardst {
    local version=${1:-$(nexo-ext-libs-git-version)}
    case $version in
        1.8.4.3)
            echo git-1.8.4.3
            ;;
            *)
            echo git-1.8.4.3
            ;;
    esac

}

function nexo-ext-libs-git-file-check-exist {
    nexo-ext-libs-file-check-exist $@
    return $?
}

# interface

function nexo-ext-libs-git-get {
    nexo-ext-libs-PKG-get git
}
function nexo-ext-libs-git-conf- {
    # begin to configure
    echo $msg make configure
    make configure || exit $?
    echo $msg ./configure --prefix=$(nexo-ext-libs-git-install-dir)
    ./configure --prefix=$(nexo-ext-libs-git-install-dir) || exit $?
}
function nexo-ext-libs-git-conf {
    nexo-ext-libs-PKG-conf git
}

function nexo-ext-libs-git-make {
    nexo-ext-libs-PKG-make git
}

function nexo-ext-libs-git-install {
    nexo-ext-libs-PKG-install git
}

function nexo-ext-libs-git-setup {
    nexo-ext-libs-PKG-setup git
}
