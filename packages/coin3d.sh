#!/bin/bash

# meta data
function nexo-ext-libs-coin3d-name {
    echo Coin
}

function nexo-ext-libs-coin3d-version-default {
    echo 3.1.3
}

function nexo-ext-libs-coin3d-version {
    echo $(nexo-ext-libs-PKG-version coin3d)
}

# dependencies
function nexo-ext-libs-coin3d-dependencies-list {
    echo
}
function nexo-ext-libs-coin3d-dependencies-setup {
    nexo-ext-libs-dependencies-setup coin3d
}

# install dir
function nexo-ext-libs-coin3d-install-dir {
    local version=${1:-$(nexo-ext-libs-coin3d-version)}
    echo $(nexo-ext-libs-install-root)/$(nexo-ext-libs-coin3d-name)/$version
}

# download info
function nexo-ext-libs-coin3d-download-url {
    local version=${1:-$(nexo-ext-libs-coin3d-version)}
    case $version in
        3.1.3)
            echo https://bitbucket.org/Coin3D/coin/downloads/Coin-3.1.3.tar.gz
            ;;
        *) 
            echo https://bitbucket.org/Coin3D/coin/downloads/Coin-3.1.3.tar.gz
            ;;
    esac
}

function nexo-ext-libs-coin3d-download-filename {
    local version=${1:-$(nexo-ext-libs-coin3d-version)}
    case $version in
        3.1.3)
            echo Coin-3.1.3.tar.gz
            ;;
        *) 
            echo Coin-3.1.3.tar.gz
            ;;
    esac
}

function nexo-ext-libs-coin3d-tardst {
    local version=${1:-$(nexo-ext-libs-coin3d-version)}
    case $version in
        3.1.3)
            echo Coin-3.1.3
            ;;
        *) 
            echo Coin-3.1.3
            ;;
    esac
}

function nexo-ext-libs-coin3d-file-check-exist {
    nexo-ext-libs-file-check-exist $@
    return $?
}

# get 
function nexo-ext-libs-coin3d-get {
    nexo-ext-libs-PKG-get coin3d
}

# conf

function nexo-ext-libs-coin3d-conf- {
    local msg="===== $FUNCNAME: "

    echo $msg ./configure --prefix=$(nexo-ext-libs-coin3d-install-dir)
    ./configure --prefix=$(nexo-ext-libs-coin3d-install-dir) || exit $?

}
function nexo-ext-libs-coin3d-conf {
    nexo-ext-libs-PKG-conf coin3d
}

# make 
function nexo-ext-libs-coin3d-make {
    nexo-ext-libs-PKG-make coin3d
}

# install
function nexo-ext-libs-coin3d-install {
    nexo-ext-libs-PKG-install coin3d
}

# setup


function nexo-ext-libs-coin3d-setup {
    nexo-ext-libs-PKG-setup coin3d
}


