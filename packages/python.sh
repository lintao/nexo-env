#!/bin/bash

function nexo-ext-libs-python-name {
    echo Python
}

function nexo-ext-libs-python-version-default {
    echo 2.7.11
}

function nexo-ext-libs-python-version {
    echo $(nexo-ext-libs-PKG-version python)
}

function nexo-ext-libs-python-install-dir {
    local version=${1:-$(nexo-ext-libs-python-version)}
    echo $(nexo-ext-libs-install-root)/$(nexo-ext-libs-python-name)/$version
}

function nexo-ext-libs-python-download-url {
    local version=${1:-$(nexo-ext-libs-python-version)}
    case $version in
        2.7.11)
            echo http://www.python.org/ftp/python/2.7.11/Python-2.7.11.tgz
            ;;
            *)
            echo http://www.python.org/ftp/python/2.7.11/Python-2.7.11.tgz
            ;;
    esac

}

function nexo-ext-libs-python-download-filename {
    local version=${1:-$(nexo-ext-libs-python-version)}
    case $version in
        2.7.11)
            echo Python-2.7.11.tgz
            ;;
            *)
            echo Python-2.7.11.tgz
            ;;
    esac

}

function nexo-ext-libs-python-tardst {
    echo $(nexo-ext-libs-python-name)-$(nexo-ext-libs-python-version) 
}

function nexo-ext-libs-python-file-check-exist {
    # if exists, return 0
    # else, return 1
    fn="$1"
    if [ -f "$fn" ]; then
        return 0
    else
        return 1
    fi
}

# interface

function nexo-ext-libs-python-get {
    nexo-ext-libs-PKG-get python
}

function nexo-ext-libs-python-conf- {
    local msg="===== $FUNCNAME: "
    echo $msg ./configure --prefix=$(nexo-ext-libs-python-install-dir) --enable-shared
    ./configure --prefix=$(nexo-ext-libs-python-install-dir) --enable-shared \
                --enable-unicode=ucs4
    if [ "$?" != "0" ]; then
        echo $msg Configure Failed.
        exit 1
    fi
}

function nexo-ext-libs-python-conf {
    nexo-ext-libs-PKG-conf python
}

function nexo-ext-libs-python-make {
    nexo-ext-libs-PKG-make python
}

function nexo-ext-libs-python-install {
    nexo-ext-libs-PKG-install python
}
# for some specific site, need to setup PYTHONPATH
function nexo-ext-libs-python-generate-sh {
    local pkg=$1
    local install=$2
    pushd $install
    # check the lib or lib64
    for lib in lib lib64
    do
        if [ ! -d $install/$lib ];
        then
            continue
        fi
        pushd $install/$lib
        find . -name lib-dynload
        local result=$(find . -name lib-dynload)
        popd
        if [ -n "$result" ]; then
cat << EOF >> bashrc
export PYTHONPATH=\${NEXO_EXTLIB_${pkg}_HOME}/$lib/$result:\$PYTHONPATH
EOF
        fi
    done
}

function nexo-ext-libs-python-generate-csh {
    local pkg=$1
    local install=$2

    # check the lib or lib64
    for lib in lib lib64
    do
        if [ ! -d $install/$lib ];
        then
            continue
        fi
        pushd $install/$lib
        find . -name lib-dynload
        local result=$(find . -name lib-dynload)
        if [ -n "$result" ]; then
cat << EOF >> tcshrc
if ( \$?PYTHONPATH == 0 ) then
    setenv PYTHONPATH ""
endif
setenv PYTHONPATH \${NEXO_EXTLIB_${pkg}_HOME}/$lib/$result:\${PYTHONPATH}
EOF
        fi
    done
}
function nexo-ext-libs-python-setup {
    nexo-ext-libs-PKG-setup python
}
