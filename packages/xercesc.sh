#!/bin/bash

# meta data
function nexo-ext-libs-xercesc-name {
    echo Xercesc
}

function nexo-ext-libs-xercesc-version-default {
    echo 3.1.2
}
function nexo-ext-libs-xercesc-version {
    echo $(nexo-ext-libs-PKG-version xercesc)
}
# dependencies
function nexo-ext-libs-xercesc-dependencies-list {
    echo 
}

function nexo-ext-libs-xercesc-dependencies-setup {
    nexo-ext-libs-dependencies-setup xercesc
}
# install dir
function nexo-ext-libs-xercesc-install-dir {
    local version=${1:-$(nexo-ext-libs-xercesc-version)}
    echo $(nexo-ext-libs-install-root)/$(nexo-ext-libs-xercesc-name)/$version
}

# download info
function nexo-ext-libs-xercesc-download-url {
    local version=${1:-$(nexo-ext-libs-xercesc-version)}
    case $version in
        3.1.1)
            #echo http://www.apache.org/dist/xerces/c/3/sources/xerces-c-3.1.1.tar.gz
            #echo http://nexo.ihep.ac.cn/software/offline/tarFiles/xerces-c-3.1.1.tar.gz
            echo http://archive.apache.org/dist/xerces/c/3/sources/xerces-c-3.1.1.tar.gz
            ;;
        3.1.2)
            echo http://archive.apache.org/dist/xerces/c/3/sources/xerces-c-3.1.2.tar.gz
            ;;
        *)
            echo http://archive.apache.org/dist/xerces/c/3/sources/xerces-c-3.1.1.tar.gz
            ;;
    esac
}
function nexo-ext-libs-xercesc-download-filename {
    local version=${1:-$(nexo-ext-libs-xercesc-version)}
    case $version in
        3.1.1)
            echo xerces-c-3.1.1.tar.gz
            ;;
        3.1.2)
            echo xerces-c-3.1.2.tar.gz
            ;;
        *)
            echo xerces-c-3.1.1.tar.gz
            ;;
    esac
}

# build info
function nexo-ext-libs-xercesc-tardst {
    local version=${1:-$(nexo-ext-libs-xercesc-version)}
    case $version in
        3.1.1)
            echo xerces-c-3.1.1
            ;;
        3.1.2)
            echo xerces-c-3.1.2
            ;;
        *)
            echo xerces-c-3.1.1
            ;;
    esac
}

function nexo-ext-libs-xercesc-file-check-exist {
    nexo-ext-libs-file-check-exist $@
    return $?
}

# interface 
function nexo-ext-libs-xercesc-get {
    nexo-ext-libs-PKG-get xercesc
}

function nexo-ext-libs-xercesc-conf- {
    local msg="===== $FUNCNAME: "

    echo $msg ./configure --prefix=$(nexo-ext-libs-xercesc-install-dir)
    ./configure --prefix=$(nexo-ext-libs-xercesc-install-dir) || exit $?

}
function nexo-ext-libs-xercesc-conf {
    nexo-ext-libs-PKG-conf xercesc
}

function nexo-ext-libs-xercesc-make {
    nexo-ext-libs-PKG-make xercesc
}

function nexo-ext-libs-xercesc-install {
    nexo-ext-libs-PKG-install xercesc
}

function nexo-ext-libs-xercesc-generate-sh {
    local pkg=$1
    local install=$2
    local lib=${3:-lib}
cat << EOF >> bashrc
export XERCESC_ROOT_DIR=\${NEXO_EXTLIB_${pkg}_HOME}
EOF
}
function nexo-ext-libs-xercesc-generate-csh {
    local pkg=$1
    local install=$2
    local lib=${3:-lib}
cat << EOF >> tcshrc
setenv XERCESC_ROOT_DIR \${NEXO_EXTLIB_${pkg}_HOME}
EOF
}

function nexo-ext-libs-xercesc-setup {
    nexo-ext-libs-PKG-setup xercesc
}
