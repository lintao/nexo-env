#!/bin/bash

# meta data
function nexo-ext-libs-swig-name {
    echo SWIG
}

function nexo-ext-libs-swig-version-default {
    echo 2.0.11
}
function nexo-ext-libs-swig-version {
    echo $(nexo-ext-libs-PKG-version swig)
}
# dependencies
function nexo-ext-libs-swig-dependencies-list {
    echo python
}
function nexo-ext-libs-swig-dependencies-setup {
    nexo-ext-libs-dependencies-setup swig
}

# install dir
function nexo-ext-libs-swig-install-dir {
    local version=${1:-$(nexo-ext-libs-swig-version)}
    echo $(nexo-ext-libs-install-root)/$(nexo-ext-libs-swig-name)/$version
}
# download info
function nexo-ext-libs-swig-download-url {
    local version=${1:-$(nexo-ext-libs-swig-version)}
    case $version in
        2.0.11)
            echo http://downloads.sourceforge.net/project/swig/swig/swig-2.0.11/swig-2.0.11.tar.gz
            ;;
            *)
            echo http://downloads.sourceforge.net/project/swig/swig/swig-2.0.11/swig-2.0.11.tar.gz
            ;;
    esac
}
function nexo-ext-libs-swig-download-filename {
    local version=${1:-$(nexo-ext-libs-swig-version)}
    case $version in
        2.0.11)
            echo swig-2.0.11.tar.gz
            ;;
            *)
            echo swig-2.0.11.tar.gz
            ;;
    esac
}
function nexo-ext-libs-swig-tardst {
    # install in root of external libraries
    local version=${1:-$(nexo-ext-libs-swig-version)}
    case $version in
        2.0.11)
            echo swig-2.0.11
            ;;
            *)
            echo swig-2.0.11
            ;;
    esac
}

function nexo-ext-libs-swig-file-check-exist {
    nexo-ext-libs-file-check-exist $@
    return $?
}

# interface
function nexo-ext-libs-swig-get {
    nexo-ext-libs-PKG-get swig
}
function nexo-ext-libs-swig-conf-uncompress {
    local msg="===== $FUNCNAME: "
    tardst=$1
    tarname=$2
    echo tar -C $tardst -zxvf $tarname
    if [ -d "$tardst" ]; then
        echo $msg $tardst already exist
        echo $msg will not invoke the uncompress command
        return
    else
        mkdir -p $tardst
    fi
    tar -zxvf $tarname || exit $?
    if [ ! -d "$tardst" ]; then
        echo $msg "After Uncompress, can't find $tardst"
        exit 1
    fi
}
function nexo-ext-libs-swig-conf- {
    local msg="===== $FUNCNAME: "
    # build pcre first
    if [ ! -f "pcre-8.33.tar.gz" ]; then
        echo $msg wget ftp://ftp.csx.cam.ac.uk/pub/software/programming/pcre/pcre-8.33.tar.gz
        wget ftp://ftp.csx.cam.ac.uk/pub/software/programming/pcre/pcre-8.33.tar.gz
    fi
    if [ ! -d "prce" ]; then
        echo $msg build prce
        bash Tools/pcre-build.sh
    fi
    pwd

    echo $msg ./configure --prefix=$(nexo-ext-libs-swig-install-dir) 
    ./configure --prefix=$(nexo-ext-libs-swig-install-dir) 
}
function nexo-ext-libs-swig-conf {
    nexo-ext-libs-PKG-conf swig
}
function nexo-ext-libs-swig-make {
    nexo-ext-libs-PKG-make swig
}
function nexo-ext-libs-swig-install {
    nexo-ext-libs-PKG-install swig
}
function nexo-ext-libs-swig-setup {
    nexo-ext-libs-PKG-setup swig
}
