#!/bin/bash

# Binding for subversion
# dependencies
function nexo-ext-libs-subversion-binding-dependencies-list {
    echo python swig subversion
}
function nexo-ext-libs-subversion-binding-dependencies-setup {
    nexo-ext-libs-dependencies-setup subversion-binding
}

# 
function nexo-ext-libs-subversion-binding-tardst {
    echo $(nexo-ext-libs-subversion-tardst)
}
function nexo-ext-libs-subversion-binding-install-dir {
    echo $(nexo-ext-libs-subversion-install-dir)
}

# interface
function nexo-ext-libs-subversion-binding-get {
    local msg="==== $FUNCNAME: "
}

function nexo-ext-libs-subversion-binding-conf {
    local msg="==== $FUNCNAME: "
    pushd $(nexo-ext-libs-build-root) >& /dev/null
    local curpkg=subversion

    tarname=$(nexo-ext-libs-${curpkg}-download-filename)
    nexo-ext-libs-${curpkg}-file-check-exist $tarname
    # does not exist, so download
    if [ "$?" != "0" ]; then
        nexo-ext-libs-${curpkg}-get
    fi
    tardst=$(nexo-ext-libs-${curpkg}-tardst)
    if [ ! -d "$tardst" ]; then
        echo $msg Please Building subversion first.
        return 1
    fi
    popd >& /dev/null
}
function nexo-ext-libs-subversion-binding-make {
    local msg="===== $FUNCNAME: "
    nexo-ext-libs-build-root-check || exit $?
    pushd $(nexo-ext-libs-build-root) >& /dev/null

    local curpkg=subversion
    local tardst=$(nexo-ext-libs-${curpkg}-tardst)
    cd $tardst/$tardst
    if [ "$?" != "0" ]; then
        echo $msg Please configure the package first.
        exit 1
    fi

    echo $msg make swig-py
    make swig-py
    echo $msg make swig-pl-lib
    make swig-pl-lib

    popd >& /dev/null
}

function nexo-ext-libs-subversion-binding-install- {
    local msg="===== $FUNCNAME: "
    cd $(nexo-ext-libs-subversion-tardst)
    echo $msg make install-swig-py
    make install-swig-py

    echo $msg make install-swig-pl-lib
    make install-swig-pl-lib
    cd subversion/bindings/swig/perl/native 
    perl Makefile.PL PREFIX=$(nexo-ext-libs-subversion-install-dir)    
    make install
}

function nexo-ext-libs-subversion-binding-install {
    nexo-ext-libs-PKG-install subversion-binding  
}

function nexo-ext-libs-subversion-binding-generate-sh {
local pkg=Subversion
local lib=lib
cat << EOF >> bashrc
export PYTHONPATH=\${NEXO_EXTLIB_${pkg}_HOME}/${lib}/svn-python:\${PYTHONPATH}
export LD_LIBRARY_PATH=\${NEXO_EXTLIB_${pkg}_HOME}/${lib}/svn-python/libsvn:\${LD_LIBRARY_PATH}
# TODO set perl lib
# only for slc5
export PERL5LIB=\${NEXO_EXTLIB_${pkg}_HOME}/lib64/perl5/site_perl/5.8.8/x86_64-linux-thread-multi:\$PERL5LIB
# only for slc6
export PERL5LIB=\${NEXO_EXTLIB_${pkg}_HOME}/lib64/perl5:\$PERL5LIB
EOF
}
function nexo-ext-libs-subversion-binding-generate-csh {
local pkg=Subversion
local lib=lib
cat << EOF >> tcshrc
if ( \$?PYTHONPATH == 0 ) then
    setenv PYTHONPATH ""
endif
setenv PYTHONPATH \${NEXO_EXTLIB_${pkg}_HOME}/${lib}/svn-python:\${PYTHONPATH}
setenv LD_LIBRARY_PATH \${NEXO_EXTLIB_${pkg}_HOME}/${lib}/svn-python/libsvn:\${LD_LIBRARY_PATH}
# TODO set perl lib 
if ( \$?PERL5LIB == 0 ) then
    setenv PERL5LIB ""
endif
# only for slc5
setenv PERL5LIB \${NEXO_EXTLIB_${pkg}_HOME}/lib64/perl5/site_perl/5.8.8/x86_64-linux-thread-multi:\$PERL5LIB
# only for slc6
setenv PERL5LIB \${NEXO_EXTLIB_${pkg}_HOME}/lib64/perl5:\$PERL5LIB
EOF
}
function nexo-ext-libs-subversion-binding-setup {
    local curpkg=subversion
    local msg="===== $FUNCNAME: "
    nexo-ext-libs-install-root-check || exit $?
    pushd $(nexo-ext-libs-install-root) >& /dev/null

    if [ ! -d "$(nexo-ext-libs-${curpkg}-install-dir)" ]; then
        echo $msg Please install the Package first
        exit 1
    fi
    local install=$(nexo-ext-libs-${curpkg}-install-dir)
    pushd $install
    nexo-ext-libs-subversion-binding-generate-sh
    nexo-ext-libs-subversion-binding-generate-csh
    popd
    popd >& /dev/null
}
