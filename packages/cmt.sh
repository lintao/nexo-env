#!/bin/bash

# meta data
function nexo-ext-libs-cmt-name {
    echo CMT
}

function nexo-ext-libs-cmt-version-default {
    echo v1r26
}
function nexo-ext-libs-cmt-version {
    echo $(nexo-ext-libs-PKG-version cmt)
}

# dependencies
function nexo-ext-libs-cmt-dependencies-list {
    echo 
}
function nexo-ext-libs-cmt-dependencies-setup {
    nexo-ext-libs-dependencies-setup cmt
}

# install dir
function nexo-ext-libs-cmt-install-dir {
    local version=${1:-$(nexo-ext-libs-cmt-version)}
    echo $(nexo-ext-libs-install-root)/$(nexo-ext-libs-cmt-name)/$version
}
# download info
function nexo-ext-libs-cmt-download-url {
    local version=${1:-$(nexo-ext-libs-cmt-version)}
    case $version in
        v1r26)
            echo http://www.cmtsite.net/v1r26/CMTv1r26.tar.gz
            ;;
        *)
            echo http://www.cmtsite.net/v1r26/CMTv1r26.tar.gz
            ;;
    esac
}
function nexo-ext-libs-cmt-download-filename {
    local version=${1:-$(nexo-ext-libs-cmt-version)}
    case $version in
        v1r26)
            echo CMTv1r26.tar.gz
            ;;
        *)
            echo CMTv1r26.tar.gz
            ;;
    esac
}

function nexo-ext-libs-cmt-tardst {
    # install in root of external libraries
    local version=${1:-$(nexo-ext-libs-cmt-version)}
    case $version in
        4.5.3)
            echo $(nexo-ext-libs-install-root)/$(nexo-ext-libs-cmt-name)/$(nexo-ext-libs-cmt-version)
            ;;
        *)
            echo $(nexo-ext-libs-install-root)/$(nexo-ext-libs-cmt-name)/$(nexo-ext-libs-cmt-version)
            ;;
    esac
}

function nexo-ext-libs-cmt-file-check-exist {
    nexo-ext-libs-file-check-exist $@
    return $?
}

# interface
function nexo-ext-libs-cmt-get {
    nexo-ext-libs-PKG-get cmt
}
function nexo-ext-libs-cmt-conf-uncompress {
    local msg="===== $FUNCNAME: "
    tardst=$1
    tarname=$2
    echo $msg tar zxvf $tarname
    tar -C $(nexo-ext-libs-install-root) -zxvf $tarname || exit $?
    if [ ! -d "$tardst" ]; then
        echo $msg "After Uncompress, can't find $tardst"
        exit 1
    fi
}

function nexo-ext-libs-cmt-conf- {
    local msg="===== $FUNCNAME: "
    
    pushd mgr
    ./INSTALL
    source setup.sh
    popd


}
function nexo-ext-libs-cmt-conf {
    nexo-ext-libs-PKG-conf cmt
}

function nexo-ext-libs-cmt-make- {
    pushd mgr
    source setup.sh
    make
    popd
}
function nexo-ext-libs-cmt-make {
    nexo-ext-libs-PKG-make cmt
}

function nexo-ext-libs-cmt-install- {
    echo
}

function nexo-ext-libs-cmt-install {
    nexo-ext-libs-PKG-install cmt
}

function nexo-ext-libs-cmt-generate-sh {
local pkg=$1
local install=$2
local lib=${3:-lib}
cat << EOF >> bashrc
source \${NEXO_EXTLIB_${pkg}_HOME}/mgr/setup.sh
EOF
}
function nexo-ext-libs-cmt-generate-csh {
local pkg=$1
local install=$2
local lib=${3:-lib}
cat << EOF >> tcshrc
source \${NEXO_EXTLIB_${pkg}_HOME}/mgr/setup.csh
EOF
}

function nexo-ext-libs-cmt-setup {
    #echo "Please use the script in CMT."
    nexo-ext-libs-PKG-setup cmt
}
