#!/bin/bash  

# meta data
function nexo-ext-libs-boost-name {
    echo Boost
}

function nexo-ext-libs-boost-version-default {
    echo 1.55.0
}
function nexo-ext-libs-boost-version {
    echo $(nexo-ext-libs-PKG-version boost)
}
function nexo-ext-libs-boost-dependencies-list {
    echo python
}
function nexo-ext-libs-boost-dependencies-setup {
    nexo-ext-libs-dependencies-setup boost
}

function nexo-ext-libs-boost-install-dir {
    local version=${1:-$(nexo-ext-libs-boost-version)}
    echo $(nexo-ext-libs-install-root)/$(nexo-ext-libs-boost-name)/$version
}

function nexo-ext-libs-boost-download-url {
    local version=${1:-$(nexo-ext-libs-boost-version)}
    case $version in
        1.55.0)
            echo http://sourceforge.net/projects/boost/files/boost/1.55.0/boost_1_55_0.tar.gz
            ;;
            *)
            echo http://sourceforge.net/projects/boost/files/boost/1.55.0/boost_1_55_0.tar.gz
            ;;
    esac

}

function nexo-ext-libs-boost-download-filename {
    local version=${1:-$(nexo-ext-libs-boost-version)}
    case $version in
        1.55.0)
            echo boost_1_55_0.tar.gz
            ;;
            *)
            echo boost_1_55_0.tar.gz
            ;;
    esac
}

function nexo-ext-libs-boost-tardst {
    local version=${1:-$(nexo-ext-libs-boost-version)}
    case $version in
        1.55.0)
            echo boost_1_55_0
            ;;
            *)
            echo boost_1_55_0
            ;;
    esac

}

function nexo-ext-libs-boost-file-check-exist {
    # if exists, return 0
    # else, return 1
    fn="$1"
    if [ -f "$fn" ]; then
        return 0
    else
        return 1
    fi
}

# interface

function nexo-ext-libs-boost-get {
    nexo-ext-libs-PKG-get boost
}

function nexo-ext-libs-boost-conf- {
    local msg="===== $FUNCNAME: "

    # TODO: it will move to the top level function
    # we need setup the dependencies first
    nexo-ext-libs-boost-dependencies-setup

    # begin to configure
    echo $msg ./bootstrap.sh --prefix=$(nexo-ext-libs-boost-install-dir) 
    ./bootstrap.sh --prefix=$(nexo-ext-libs-boost-install-dir) 
    if [ "$?" != "0" ]; then
        echo $msg Configure Failed.
        exit 1
    fi
}

function nexo-ext-libs-boost-conf {
    nexo-ext-libs-PKG-conf boost
}

function nexo-ext-libs-boost-make- {
    local msg="===== $FUNCNAME: "
    nexo-ext-libs-boost-dependencies-setup

    echo $msg ./b2 install
    ./b2 install
}

function nexo-ext-libs-boost-make {
    nexo-ext-libs-PKG-make boost
}

function nexo-ext-libs-boost-install- {
    echo $msg ./b2 install
    ./b2 install
}
function nexo-ext-libs-boost-install {
    nexo-ext-libs-PKG-install boost
}

function nexo-ext-libs-boost-setup {
    nexo-ext-libs-PKG-setup boost
}
