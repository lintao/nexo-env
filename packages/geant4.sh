#!/bin/bash
# this script will be a little complicated.
# meta data
function nexo-ext-libs-geant4-name {
    echo Geant4
}

function nexo-ext-libs-geant4-version-default {
    #echo 9.4.p04
    #echo 10.02.p01
    #echo 10.02.p01
    echo 10.00.p01
    #echo 10.00.p04
}
function nexo-ext-libs-geant4-version {
    echo $(nexo-ext-libs-PKG-version geant4)
}
# dependencies
function nexo-ext-libs-geant4-dependencies-list {
    echo python boost cmake xercesc +qt4 ROOT coin3d coinsoxt
    #echo clhep
}
function nexo-ext-libs-geant4-dependencies-setup {
    nexo-ext-libs-dependencies-setup geant4
}
# install dir
function nexo-ext-libs-geant4-install-dir {
    local version=${1:-$(nexo-ext-libs-geant4-version)}
    echo $(nexo-ext-libs-install-root)/$(nexo-ext-libs-geant4-name)/$version
}


# download related
function nexo-ext-libs-geant4-official-download-url {
    echo http://geant4.cern.ch/support/source/
}

## data list
function nexo-ext-libs-geant4-data-list-9.4 {
    # if seq is "", it means this is the directory for data
    # elseif seq is ".", it means this is the tarball
    local seq=${1:-}
    echo G4NDL${seq}3.14
    echo G4EMLOW${seq}6.19
    if [ "${seq}" = "" ]; then
        echo PhotonEvaporation${seq}2.1
    else
        echo G4PhotonEvaporation${seq}2.1
    fi
    if [ "${seq}" = "" ]; then
        echo RadioactiveDecay${seq}3.3
    else
        echo G4RadioactiveDecay${seq}3.3
    fi
    echo G4ABLA${seq}3.0
    echo G4NEUTRONXS${seq}1.0
    echo G4PII${seq}1.2
    echo RealSurface${seq}1.0
}

function nexo-ext-libs-geant4-data-list {
    local version=${1:-$(nexo-ext-libs-geant4-version)}
    local seq=${2:-}

    case $version in
        9.4*)
            echo $(nexo-ext-libs-geant4-data-list-9.4 $seq)
            ;;
        *)
            echo $(nexo-ext-libs-geant4-data-list-9.4 $seq)
            ;;
    esac
}
function nexo-ext-libs-geant4-get-data-dir {
    local msg="===== $FUNCNAME: "
    local version=${1:-$(nexo-ext-libs-geant4-version)}
    echo $(nexo-ext-libs-geant4-install-dir $version)/data-archive
}
function nexo-ext-libs-geant4-get-data {
    local msg="===== $FUNCNAME: "
    echo $msg Download data for Geant4
    local version=${1:-$(nexo-ext-libs-geant4-version)}
    local dataname=""
    datadir=$(nexo-ext-libs-geant4-get-data-dir $version)
    test -d $datadir || mkdir -p $datadir
    pushd $datadir >& /dev/null
    for dataname in $(nexo-ext-libs-geant4-data-list $version ".")
    do
        if [ ! -f "${dataname}.tar.gz" ]; then
            echo $msg wget $(nexo-ext-libs-geant4-official-download-url)/${dataname}.tar.gz
            if [ "$(nexo-archive-check)" == "0" ]; then 
                wget $(nexo-ext-libs-geant4-official-download-url)/${dataname}.tar.gz || exit $?
            else
                nexo-archive-get ${dataname}.tar.gz ${dataname}.tar.gz || exit $?
            fi
        else
            echo $msg ${datadir}/${dataname}.tar.gz already exists
        fi
    done
    popd
}
function nexo-ext-libs-geant4-install-data-dir {
    local msg="===== $FUNCNAME: "
    local version=${1:-$(nexo-ext-libs-geant4-version)}
    echo $(nexo-ext-libs-geant4-install-dir $version)/data
}
function nexo-ext-libs-geant4-install-data {
    local msg="===== $FUNCNAME: "
    echo $msg Install data for Geant4
    local version=${1:-$(nexo-ext-libs-geant4-version)}
    local dataname=""
    installdir=$(nexo-ext-libs-geant4-install-data-dir $version)
    [ -d "$installdir" ] || mkdir -p "$installdir"
    datadir=$(nexo-ext-libs-geant4-get-data-dir $version)
    test -d $datadir || mkdir -p $datadir
    pushd $datadir >& /dev/null
    # new: unzip and check
    local tmptarsname=($(nexo-ext-libs-geant4-data-list $version "."))
    local tmpdstname=($(nexo-ext-libs-geant4-data-list $version))
    local count=${#tmptarsname[@]}
    local i=""
    for i in $(seq 1 $count)
    do
        local dataname=${tmptarsname[$i-1]}
        local installdatadir=${tmpdstname[$i-1]}
        if [ ! -f "${dataname}.tar.gz" ]; then
            echo $msg ${datadir}/${dataname}.tar.gz does not exist
            echo $msg Please get the data first
            exit 1
        else
            if [ -d "$installdir/$installdatadir" ]; then
                echo $msg Skip Uncompress the data ${datadir}/${dataname}.tar.gz
                echo $msg "$installdir/$installdatadir" already exists
            else
                echo $msg ${datadir}/${dataname}.tar.gz already exists
                echo $msg Uncompress the data ${datadir}/${dataname}.tar.gz
                tar -C $installdir -zxvf ${dataname}.tar.gz
            fi
        fi
    done
    popd
    # check again
    pushd $installdir >& /dev/null
    local installdatadir=""
    for installdatadir in $(nexo-ext-libs-geant4-data-list $version)
    do
        if [ ! -d "$installdatadir" ]; then
            echo $msg It seems the data does not exist?
            echo $msg Please check $installdatadir
            exit 1
        else
            echo $msg CHECK $installdatadir: OK
        fi
    done
    popd
}

## source code
function nexo-ext-libs-geant4-download-url {
    local version=${1:-$(nexo-ext-libs-geant4-version)}
    case $version in
        9.4.p04)
            echo $(nexo-ext-libs-geant4-official-download-url)/geant4.9.4.p04.tar.gz
            ;;
        *)
            echo $(nexo-ext-libs-geant4-official-download-url)/geant4.${version}.tar.gz
            ;;
    esac
}
function nexo-ext-libs-geant4-download-filename {
    local version=${1:-$(nexo-ext-libs-geant4-version)}
    case $version in
        9.4.p04)
            echo geant4.9.4.p04.tar.gz
            ;;
        *)
            echo geant4.${version}.tar.gz
            ;;
    esac
}
function nexo-ext-libs-geant4-tardst {
    local version=${1:-$(nexo-ext-libs-geant4-version)}
    case $version in
        9.4.p04)
            echo geant4.9.4.p04
            ;;
        *)
            echo geant4.${version}
            ;;
    esac
}

function nexo-ext-libs-geant4-file-check-exist {
    nexo-ext-libs-file-check-exist $@
    return $?
}

# interface
function nexo-ext-libs-geant4-get {
    nexo-ext-libs-PKG-get geant4
    #nexo-ext-libs-geant4-get-data 
}

function nexo-ext-libs-geant4-conf-uncompress {
    local msg="===== $FUNCNAME: "
    local tardst=$1
    local tarname=$2
    echo tar zxvf $tarname
    tar zxvf $tarname || exit 1
    if [ ! -d "$tardst" ]; then
        echo $msg "After Uncompress, can't find $tardst"
        exit 1
    fi
}

function nexo-ext-libs-geant4-is-qt-ok {
    qmake-qt4 -v 2>/dev/null 1>&2
    local st=$?
    echo $st
}

function nexo-ext-libs-geant4-conf-use-qt {
    # for some users, they don't want to use qt4.
    # * use qmake to check the qt4 is installed
    local st=$(nexo-ext-libs-geant4-is-qt-ok)
    if [ "$st" == "0" ];
    then
        # qt may exist
        echo "-DGEANT4_USE_QT=ON"
    else
        echo "-DGEANT4_USE_QT=OFF"
    fi
}

function nexo-ext-libs-geant4-conf-9.4 {
    local msg="===== $FUNCNAME: "
    echo $msg cmake .. -DCMAKE_INSTALL_PREFIX=$(nexo-ext-libs-geant4-install-dir) \
        -DGEANT4_USE_GDML=ON \
        $(nexo-ext-libs-geant4-conf-use-qt) \
        -DXERCESC_ROOT_DIR=$(nexo-ext-libs-xercesc-install-dir)
    cmake .. -DCMAKE_INSTALL_PREFIX=$(nexo-ext-libs-geant4-install-dir) \
        -DGEANT4_USE_GDML=ON \
        $(nexo-ext-libs-geant4-conf-use-qt) \
        -DXERCESC_ROOT_DIR=$(nexo-ext-libs-xercesc-install-dir)
    local st=$?
    echo $msg $st 1>&2
    if [ "$st" != "0" ]; then
        exit 1
    fi
}

function nexo-ext-libs-geant4-conf-10 {
    local msg="===== $FUNCNAME: "
    echo $msg cmake .. -DCMAKE_INSTALL_PREFIX=$(nexo-ext-libs-geant4-install-dir) \
        -DGEANT4_USE_GDML=ON \
        $(nexo-ext-libs-geant4-conf-use-qt) \
        -DXERCESC_ROOT_DIR=$(nexo-ext-libs-xercesc-install-dir)
    cmake .. -DCMAKE_INSTALL_PREFIX=$(nexo-ext-libs-geant4-install-dir) \
        -DGEANT4_USE_GDML=ON \
        -DGEANT4_INSTALL_DATA=ON \
        -DGEANT4_USE_OPENGL_X11=ON \
        -DGEANT4_USE_INVENTOR=ON \
        -DGEANT4_USE_RAYTRACER_X11=ON \
        $(nexo-ext-libs-geant4-conf-use-qt) \
        -DXERCESC_ROOT_DIR=$(nexo-ext-libs-xercesc-install-dir)
    local st=$?
    echo $msg $st 1>&2
    if [ "$st" != "0" ]; then
        exit 1
    fi
}

function nexo-ext-libs-geant4-conf- {
    local msg="===== $FUNCNAME: "
    test -d geant4-build || mkdir geant4-build
    pushd geant4-build
    local version=$(nexo-ext-libs-geant4-version)
    case $version in
        9.4*)
            echo $msg configure geant4 9.4
            nexo-ext-libs-geant4-conf-9.4
            ;;
        10.*)
            echo $msg configure geant4 10.
            nexo-ext-libs-geant4-conf-10
            ;;
        *)
            echo $msg unknown
            exit 1
    esac
    popd

}
function nexo-ext-libs-geant4-conf {
    nexo-ext-libs-PKG-conf geant4 
}

function nexo-ext-libs-geant4-make- {
    local msg="===== $FUNCNAME: "
    pushd geant4-build
    echo $msg make $(nexo-ext-libs-gen-cpu-num)
    make $(nexo-ext-libs-gen-cpu-num)
    popd
}
function nexo-ext-libs-geant4-make {
    nexo-ext-libs-PKG-make geant4
}
function nexo-ext-libs-geant4-install- {
    local msg="===== $FUNCNAME: "
    pushd geant4-build
    echo $msg make install
    make install
    popd
}

function nexo-ext-libs-geant4-install {
    nexo-ext-libs-PKG-install geant4
    #nexo-ext-libs-geant4-install-data
}

# generate the sh/csh scripts

function nexo-ext-libs-geant4-generate-sh-9.4 {
    local pkg=$1
    local install=$2
    local lib=${3:-lib}
    
cat << EOF >> bashrc
source \${NEXO_EXTLIB_${pkg}_HOME}/share/geant4-9.4.4/config/geant4-9.4.4.sh
export G4LIB_BUILD_SHARED=1
export G4ABLADATA=\${NEXO_EXTLIB_${pkg}_HOME}/data/G4ABLA3.0
export G4LEDATA=\${NEXO_EXTLIB_${pkg}_HOME}/data/G4EMLOW6.19
export G4LEVELGAMMADATA=\${NEXO_EXTLIB_${pkg}_HOME}/data/PhotonEvaporation2.1
export G4NEUTRONHPDATA=\${NEXO_EXTLIB_${pkg}_HOME}/data/G4NDL3.14
export G4NEUTRONXSDATA=\${NEXO_EXTLIB_${pkg}_HOME}/data/G4NEUTRONXS1.0
export G4PIIDATA=\${NEXO_EXTLIB_${pkg}_HOME}/data/G4PII1.2
export G4RADIOACTIVEDATA=\${NEXO_EXTLIB_${pkg}_HOME}/data/RadioactiveDecay3.3
export G4REALSURFACEDATA=\${NEXO_EXTLIB_${pkg}_HOME}/data/RealSurface1.0
EOF
    # make sure the Qt4 is setup.
    local st=$(nexo-ext-libs-geant4-is-qt-ok)
    if [ "$st" == "0" ]; then
cat << EOF >> bashrc
export CPATH=\$(qmake-qt4 -query QT_INSTALL_HEADERS):\$CPATH
EOF

    fi
}
function nexo-ext-libs-geant4-generate-csh-9.4 {
    local pkg=$1
    local install=$2
    local lib=${3:-lib}

cat << EOF >> tcshrc
source \${NEXO_EXTLIB_${pkg}_HOME}/share/geant4-9.4.4/config/geant4-9.4.4.csh
setenv G4LIB_BUILD_SHARED 1
setenv G4ABLADATA \${NEXO_EXTLIB_${pkg}_HOME}/data/G4ABLA3.0
setenv G4LEDATA \${NEXO_EXTLIB_${pkg}_HOME}/data/G4EMLOW6.19
setenv G4LEVELGAMMADATA \${NEXO_EXTLIB_${pkg}_HOME}/data/PhotonEvaporation2.1
setenv G4NEUTRONHPDATA \${NEXO_EXTLIB_${pkg}_HOME}/data/G4NDL3.14
setenv G4NEUTRONXSDATA \${NEXO_EXTLIB_${pkg}_HOME}/data/G4NEUTRONXS1.0
setenv G4PIIDATA \${NEXO_EXTLIB_${pkg}_HOME}/data/G4PII1.2
setenv G4RADIOACTIVEDATA \${NEXO_EXTLIB_${pkg}_HOME}/data/RadioactiveDecay3.3
setenv G4REALSURFACEDATA \${NEXO_EXTLIB_${pkg}_HOME}/data/RealSurface1.0
EOF
    # make sure the Qt4 is setup.
    local st=$(nexo-ext-libs-geant4-is-qt-ok)
    if [ "$st" == "0" ]; then
cat << EOF >> tcshrc
setenv CPATH \`qmake-qt4 -query QT_INSTALL_HEADERS\`:\${CPATH}
EOF

    fi
}

# for 10.x
function nexo-ext-libs-geant4-generate-sh-10 {
    local pkg=$1
    local install=$2
    local lib=${3:-lib}
    
cat << EOF >> bashrc
source \${NEXO_EXTLIB_${pkg}_HOME}/bin/geant4.sh
source \${NEXO_EXTLIB_${pkg}_HOME}/share/Geant4-10.0.1/geant4make/geant4make.sh
export G4LIB_BUILD_SHARED=1

EOF
    # make sure the Qt4 is setup.
    local st=$(nexo-ext-libs-geant4-is-qt-ok)
    if [ "$st" == "0" ]; then
cat << EOF >> bashrc
export CPATH=\$(qmake-qt4 -query QT_INSTALL_HEADERS):\$CPATH
EOF

    fi
}
function nexo-ext-libs-geant4-generate-csh-10 {
    local pkg=$1
    local install=$2
    local lib=${3:-lib}

cat << EOF >> tcshrc
source \${NEXO_EXTLIB_${pkg}_HOME}/bin/geant4.csh
source \${NEXO_EXTLIB_${pkg}_HOME}/share/Geant4-10.0.1/geant4make/geant4make.csh
setenv G4LIB_BUILD_SHARED 1
EOF
    # make sure the Qt4 is setup.
    local st=$(nexo-ext-libs-geant4-is-qt-ok)
    if [ "$st" == "0" ]; then
cat << EOF >> tcshrc
setenv CPATH \`qmake-qt4 -query QT_INSTALL_HEADERS\`:\${CPATH}
EOF

    fi
}



# generate general
function nexo-ext-libs-geant4-generate-sh {
    local version=$(nexo-ext-libs-geant4-version)
    case $version in
        9.4*)
            echo $msg setup geant4 9.4
            nexo-ext-libs-geant4-generate-sh-9.4 $@
            ;;
        10.*)
            echo $msg setup geant4 9.4
            nexo-ext-libs-geant4-generate-sh-10 $@
            ;;
        *)
            echo $msg unknown version $version
            exit 1
    esac
}
function nexo-ext-libs-geant4-generate-csh {
    local version=$(nexo-ext-libs-geant4-version)
    case $version in
        9.4*)
            echo $msg setup geant4 9.4
            nexo-ext-libs-geant4-generate-csh-9.4 $@
            ;;
        10.*)
            echo $msg setup geant4 9.4
            nexo-ext-libs-geant4-generate-csh-10 $@
            ;;
        *)
            echo $msg unknown version $version
            exit 1
    esac
}

function nexo-ext-libs-geant4-setup {
    nexo-ext-libs-PKG-setup geant4
}
