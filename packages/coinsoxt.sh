#!/bin/bash

# meta data
function nexo-ext-libs-coinsoxt-name {
    echo SoXt
}

function nexo-ext-libs-coinsoxt-version-default {
    echo 1.3.0
}

function nexo-ext-libs-coinsoxt-version {
    echo $(nexo-ext-libs-PKG-version coinsoxt)
}

# dependencies
function nexo-ext-libs-coinsoxt-dependencies-list {
    echo coin3d
}
function nexo-ext-libs-coinsoxt-dependencies-setup {
    nexo-ext-libs-dependencies-setup coinsoxt
}

# install dir
function nexo-ext-libs-coinsoxt-install-dir {
    local version=${1:-$(nexo-ext-libs-coinsoxt-version)}
    echo $(nexo-ext-libs-install-root)/$(nexo-ext-libs-coinsoxt-name)/$version
}

# download info
function nexo-ext-libs-coinsoxt-download-url {
    local version=${1:-$(nexo-ext-libs-coinsoxt-version)}
    case $version in
        *) 
            echo https://bitbucket.org/Coin3D/soxt/get/SoXt-${version}.tar.gz
            ;;
    esac
}

function nexo-ext-libs-coinsoxt-download-filename {
    local version=${1:-$(nexo-ext-libs-coinsoxt-version)}
    case $version in
        1.3.0)
            echo SoXt-1.3.0.tar.gz
            ;;
        *) 
            echo SoXt-1.3.0.tar.gz
            ;;
    esac
}

function nexo-ext-libs-coinsoxt-tardst {
    local version=${1:-$(nexo-ext-libs-coinsoxt-version)}
    case $version in
        1.3.0)
            echo Coin3D-soxt-47ebb4c4dd07
            ;;
        *) 
            echo Coin3D-soxt-47ebb4c4dd07
            ;;
    esac
}

function nexo-ext-libs-coinsoxt-file-check-exist {
    nexo-ext-libs-file-check-exist $@
    return $?
}

# get 
function nexo-ext-libs-coinsoxt-get {
    nexo-ext-libs-PKG-get coinsoxt
}

# conf

function nexo-ext-libs-coinsoxt-conf- {
    local msg="===== $FUNCNAME: "

    if [ ! -d "src/Inventor/Xt/common" ] ; then
        wget https://bitbucket.org/Coin3D/sogui/get/229ba2f86292.zip
        unzip 229ba2f86292.zip
        mv Coin3D-sogui-229ba2f86292 src/Inventor/Xt/common
    fi

    echo $msg ./configure --prefix=$(nexo-ext-libs-coinsoxt-install-dir)
    ./configure --prefix=$(nexo-ext-libs-coinsoxt-install-dir) || exit $?

}
function nexo-ext-libs-coinsoxt-conf {
    nexo-ext-libs-PKG-conf coinsoxt
}

# make 
function nexo-ext-libs-coinsoxt-make {
    nexo-ext-libs-PKG-make coinsoxt
}

# install
function nexo-ext-libs-coinsoxt-install {
    nexo-ext-libs-PKG-install coinsoxt
}

# setup


function nexo-ext-libs-coinsoxt-setup {
    nexo-ext-libs-PKG-setup coinsoxt
}


