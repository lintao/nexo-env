#!/bin/bash

function nexo-ext-libs-libmore-name {
    echo libmore
}

function nexo-ext-libs-libmore-version-default {
    echo 0.8.3
}

function nexo-ext-libs-libmore-version {
    echo $(nexo-ext-libs-PKG-version libmore)
}

# dependencies
function nexo-ext-libs-libmore-dependencies-list {
    echo
}
function nexo-ext-libs-libmore-dependencies-setup {
    nexo-ext-libs-dependencies-setup libmore
}

# install dir
function nexo-ext-libs-libmore-install-dir {
    local version=${1:-$(nexo-ext-libs-libmore-version)}
    echo $(nexo-ext-libs-install-root)/$(nexo-ext-libs-libmore-name)/$version
}

# download info
function nexo-ext-libs-libmore-download-url {
    local version=${1:-$(nexo-ext-libs-libmore-version)}
    case $version in
        0.8.3)
            echo http://heanet.dl.sourceforge.net/project/more/more/0.8.3/more-0.8.3.tar.bz2
            ;;
        *) 
            echo http://heanet.dl.sourceforge.net/project/more/more/0.8.3/more-0.8.3.tar.bz2
            ;;
    esac
}

function nexo-ext-libs-libmore-download-filename {
    local version=${1:-$(nexo-ext-libs-libmore-version)}
    case $version in
        0.8.3)
            echo more-0.8.3.tar.bz2
            ;;
        *) 
            echo more-0.8.3.tar.bz2
            ;;
    esac
}

function nexo-ext-libs-libmore-tardst {
    local version=${1:-$(nexo-ext-libs-libmore-version)}
    case $version in
        0.8.3)
            echo more-0.8.3
            ;;
        *) 
            echo more-0.8.3
            ;;
    esac
}

function nexo-ext-libs-libmore-file-check-exist {
    nexo-ext-libs-file-check-exist $@
    return $?
}

# get 
function nexo-ext-libs-libmore-get {
    nexo-ext-libs-PKG-get libmore
}

# conf

function nexo-ext-libs-libmore-get-patch-name {
    echo $(nexo-ext-libs-init-scripts-dir)/patches/$(nexo-ext-libs-libmore-name)-$(nexo-ext-libs-libmore-version).patch
}

function nexo-ext-libs-libmore-apply-patch {
    local msg="===== $FUNCNAME: "
    # load patch file name
    local patchname=$(nexo-ext-libs-libmore-get-patch-name)
    echo $msg Apply Patch $patchname
    # apply patch
    patch -p1 < $patchname
}
function nexo-ext-libs-libmore-conf- {
    local msg="===== $FUNCNAME: "

    # load patch for libmore
    nexo-ext-libs-libmore-apply-patch
    echo $msg ./configure --prefix=$(nexo-ext-libs-libmore-install-dir)

    local tardst=$(nexo-ext-libs-build-root)/$(nexo-ext-libs-libmore-tardst)

    [ -d "temp-build" ] || mkdir temp-build
    pushd temp-build
    $tardst/configure --prefix=$(nexo-ext-libs-libmore-install-dir) || exit $?
    popd

}
function nexo-ext-libs-libmore-conf {
    nexo-ext-libs-PKG-conf libmore
}

# make 
function nexo-ext-libs-libmore-make- {
    pushd temp-build
    make
    popd
}
function nexo-ext-libs-libmore-make {
    nexo-ext-libs-PKG-make libmore
}

# install
function nexo-ext-libs-libmore-install- {
    pushd temp-build
    make install
    popd
}
function nexo-ext-libs-libmore-install {
    nexo-ext-libs-PKG-install libmore
}

# setup


function nexo-ext-libs-libmore-setup {
    nexo-ext-libs-PKG-setup libmore
}
