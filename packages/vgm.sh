#!/bin/bash

# meta data
function nexo-ext-libs-vgm-name {
    echo vgm
}

function nexo-ext-libs-vgm-version-default {
    echo 4.3
}

function nexo-ext-libs-vgm-version {
    echo $(nexo-ext-libs-PKG-version vgm)
}

# dependencies
function nexo-ext-libs-vgm-dependencies-list {
    echo geant4 ROOT
}
function nexo-ext-libs-vgm-dependencies-setup {
    nexo-ext-libs-dependencies-setup vgm
}

# install dir
function nexo-ext-libs-vgm-install-dir {
    local version=${1:-$(nexo-ext-libs-vgm-version)}
    echo $(nexo-ext-libs-install-root)/$(nexo-ext-libs-vgm-name)/$version
}

# download info
function nexo-ext-libs-vgm-download-url {
    local version=${1:-$(nexo-ext-libs-vgm-version)}
    case $version in
        *) 
            echo http://ivana.home.cern.ch/ivana/vgm.${version}.tar.gz
            ;;
    esac
}

function nexo-ext-libs-vgm-download-filename {
    local version=${1:-$(nexo-ext-libs-vgm-version)}
    case $version in
        *) 
            echo vgm.${version}.tar.gz
            ;;
    esac
}

function nexo-ext-libs-vgm-tardst {
    local version=${1:-$(nexo-ext-libs-vgm-version)}
    case $version in
        *) 
            echo vgm.${version}
            ;;
    esac
}

function nexo-ext-libs-vgm-file-check-exist {
    nexo-ext-libs-file-check-exist $@
    return $?
}

# get 
function nexo-ext-libs-vgm-get {
    nexo-ext-libs-PKG-get vgm
}

# conf

function nexo-ext-libs-vgm-conf- {
    local msg="===== $FUNCNAME: "

    [ -d build ] || mkdir build || exit -1
    pushd build
    cmake .. -DCMAKE_INSTALL_PREFIX=$(nexo-ext-libs-vgm-install-dir)
    popd

}
function nexo-ext-libs-vgm-conf {
    nexo-ext-libs-PKG-conf vgm
}

# make 
function nexo-ext-libs-vgm-make- {
    pushd build
    make
    popd
}
function nexo-ext-libs-vgm-make {
    nexo-ext-libs-PKG-make vgm
}

# install
function nexo-ext-libs-vgm-install- {
    pushd build
    make install
    popd
}
function nexo-ext-libs-vgm-install {
    nexo-ext-libs-PKG-install vgm
}

# setup


function nexo-ext-libs-vgm-setup {
    nexo-ext-libs-PKG-setup vgm
}


