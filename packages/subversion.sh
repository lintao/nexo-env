#!/bin/bash

# meta data
function nexo-ext-libs-subversion-name {
    echo Subversion
}

function nexo-ext-libs-subversion-version-default {
    echo 1.6.23
}
function nexo-ext-libs-subversion-version {
    echo $(nexo-ext-libs-PKG-version subversion)
}

# dependencies
function nexo-ext-libs-subversion-dependencies-list {
    echo python swig
}
function nexo-ext-libs-subversion-dependencies-setup {
    nexo-ext-libs-dependencies-setup subversion
}

# install dir
function nexo-ext-libs-subversion-install-dir {
    local version=${1:-$(nexo-ext-libs-subversion-version)}
    echo $(nexo-ext-libs-install-root)/$(nexo-ext-libs-subversion-name)/$version
}

# download info
function nexo-ext-libs-subversion-download-url {
    local version=${1:-$(nexo-ext-libs-subversion-version)}
    case $version in
        1.6.23)
            echo http://archive.apache.org/dist/subversion/subversion-1.6.23.tar.gz
            ;;
            *)
            echo http://archive.apache.org/dist/subversion/subversion-1.6.23.tar.gz
            ;;
    esac
}
function nexo-ext-libs-subversion-download-filename {
    local version=${1:-$(nexo-ext-libs-subversion-version)}
    case $version in
        1.6.23)
            echo subversion-1.6.23.tar.gz
            ;;
            *)
            echo subversion-1.6.23.tar.gz
            ;;
    esac
}
function nexo-ext-libs-subversion-tardst {
    local version=${1:-$(nexo-ext-libs-subversion-version)}
    case $version in
        1.6.23)
            echo subversion-1.6.23
            ;;
            *)
            echo subversion-1.6.23
            ;;
    esac
}
function nexo-ext-libs-subversion-file-check-exist {
    nexo-ext-libs-file-check-exist $@
    return $?
}

# interface
function nexo-ext-libs-subversion-get {
    nexo-ext-libs-PKG-get subversion
}

function nexo-ext-libs-subversion-conf-uncompress {
    local msg="===== $FUNCNAME: "
    tardst=$1
    tarname=$2
    echo tar -C $tardst -zxvf $tarname
    if [ -d "$tardst" ]; then
        echo $msg $tardst already exist
        echo $msg will not invoke the uncompress command
        return
    else
        mkdir -p $tardst
    fi
    tar -C $tardst -zxvf $tarname || exit $?
    if [ ! -d "$tardst" ]; then
        echo $msg "After Uncompress, can't find $tardst"
        exit 1
    fi
}
function nexo-ext-libs-subversion-conf- {
    local msg="===== $FUNCNAME: "
    # get the dependency first
    cd $(nexo-ext-libs-subversion-tardst)
    ls 

    # set up the version of dependencies
    if [ ! -f "get-deps.sh.orig" ];
    then
        cp get-deps.sh{,.orig}
        sed -i 's/NEON=neon-0.28.3/NEON=neon-0.30.0/' get-deps.sh
    fi

    # download dependency
    local downloadpkgforsvn=""
    for downloadpkgforsvn in neon zlib serf sqlite-amalgamation 
    do
        echo $msg Check Dependency Package $downloadpkgforsvn
        if [ ! -d $downloadpkgforsvn ]; then
            bash get-deps.sh
        fi
    done
    echo $msg ./configure --prefix=$(nexo-ext-libs-subversion-install-dir) --enable-shared --with-ssl
    ./configure --prefix=$(nexo-ext-libs-subversion-install-dir) \
                --enable-shared --with-ssl --with-gnome-keyring \
                --disable-mod-activation
}
function nexo-ext-libs-subversion-conf {
    nexo-ext-libs-PKG-conf subversion
}
function nexo-ext-libs-subversion-make- {
    local msg="===== $FUNCNAME: "
    cd $(nexo-ext-libs-subversion-tardst)/
    echo $msg make $(nexo-ext-libs-gen-cpu-num)
    make $(nexo-ext-libs-gen-cpu-num)
}
function nexo-ext-libs-subversion-make {
    nexo-ext-libs-PKG-make subversion
}
function nexo-ext-libs-subversion-install- {
    local msg="===== $FUNCNAME: "
    cd $(nexo-ext-libs-subversion-tardst)
    echo $msg make install
    make install
}

function nexo-ext-libs-subversion-install {
    nexo-ext-libs-PKG-install subversion 
}

function nexo-ext-libs-subversion-setup {
    nexo-ext-libs-PKG-setup subversion
}
