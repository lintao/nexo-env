#!/bin/bash

# meta data
function nexo-ext-libs-qt4-name {
    echo Qt4
}

function nexo-ext-libs-qt4-version-default {
    # in lxslc5, we should use 4.5.3
    # otherwise, please use apt-get or yum?
    echo 4.5.3
}

function nexo-ext-libs-qt4-version {
    echo $(nexo-ext-libs-PKG-version qt4)
}

# dependencies
function nexo-ext-libs-qt4-dependencies-list {
    echo python boost
}
function nexo-ext-libs-qt4-dependencies-setup {
    nexo-ext-libs-dependencies-setup qt4
}

# install dir
function nexo-ext-libs-qt4-install-dir {
    local version=${1:-$(nexo-ext-libs-qt4-version)}
    echo $(nexo-ext-libs-install-root)/$(nexo-ext-libs-qt4-name)/$version
}

# download info
function nexo-ext-libs-qt4-download-url {
    local version=${1:-$(nexo-ext-libs-qt4-version)}
    case $version in
        4.5.3)
            echo http://download.qt-project.org/archive/qt/4.5/qt-x11-opensource-src-4.5.3.tar.gz
            ;;
        *)
            echo http://download.qt-project.org/archive/qt/4.5/qt-x11-opensource-src-4.5.3.tar.gz
            ;;
    esac
}
function nexo-ext-libs-qt4-download-filename {
    local version=${1:-$(nexo-ext-libs-qt4-version)}
    case $version in
        4.5.3)
            echo qt-x11-opensource-src-4.5.3.tar.gz
            ;;
        *)
            echo qt-x11-opensource-src-4.5.3.tar.gz
            ;;
    esac
}

function nexo-ext-libs-qt4-tardst {
    local version=${1:-$(nexo-ext-libs-qt4-version)}
    case $version in
        4.5.3)
            echo qt-x11-opensource-src-4.5.3
            ;;
        *)
            echo qt-x11-opensource-src-4.5.3
            ;;
    esac
}

function nexo-ext-libs-qt4-file-check-exist {
    nexo-ext-libs-file-check-exist $@
    return $?
}

# interface 
function nexo-ext-libs-qt4-get {
    nexo-ext-libs-PKG-get qt4
}
function nexo-ext-libs-qt4-conf-uncompress {
    local msg="===== $FUNCNAME: "
    tardst=$1
    tarname=$2
    echo $msg tar zxvf $tarname
    tar zxvf $tarname || exit $?
    if [ ! -d "$tardst" ]; then
        echo $msg "After Uncompress, can't find $tardst"
        exit 1
    fi
}
function nexo-ext-libs-qt4-conf- {
    local msg="===== $FUNCNAME: "

    echo $msg ./configure -no-separate-debug-info -release -prefix $(nexo-ext-libs-qt4-install-dir) -confirm-license -opensource -nomake examples -nomake demos
    ./configure -no-separate-debug-info -release -prefix $(nexo-ext-libs-qt4-install-dir) -confirm-license -opensource -nomake examples -nomake demos

}
function nexo-ext-libs-qt4-conf {
    nexo-ext-libs-PKG-conf qt4
}

function nexo-ext-libs-qt4-make {
    nexo-ext-libs-PKG-make qt4
}

function nexo-ext-libs-qt4-install {
    nexo-ext-libs-PKG-install qt4
}

# set QTHOME for geant4.
function nexo-ext-libs-qt4-generate-sh {
local pkg=$1
local install=$2
local lib=${3:-lib}
cat << EOF >> bashrc
export QTHOME=\${NEXO_EXTLIB_${pkg}_HOME}
EOF
}
function nexo-ext-libs-qt4-generate-csh {
local pkg=$1
local install=$2
local lib=${3:-lib}
cat << EOF >> tcshrc
setenv QTHOME \${NEXO_EXTLIB_${pkg}_HOME}
EOF
}
function nexo-ext-libs-qt4-setup {
    nexo-ext-libs-PKG-setup qt4
}
