#!/bin/bash

# meta data
function nexo-ext-libs-gccxml-name {
    echo GCCXML
}

function nexo-ext-libs-gccxml-version-default {
    # TODO
    # I don't know the version
    echo master
}
function nexo-ext-libs-gccxml-version {
    echo $(nexo-ext-libs-PKG-version gccxml)
}

# dependencies
function nexo-ext-libs-gccxml-dependencies-list {
    echo cmake
}

function nexo-ext-libs-gccxml-dependencies-setup {
    nexo-ext-libs-dependencies-setup gccxml
}

# install dir
function nexo-ext-libs-gccxml-install-dir {
    local version=${1:-$(nexo-ext-libs-gccxml-version)}
    echo $(nexo-ext-libs-install-root)/$(nexo-ext-libs-gccxml-name)/$version
}

# download info
function nexo-ext-libs-gccxml-download-url {
    local version=${1:-$(nexo-ext-libs-gccxml-version)}
    case $version in
        master)
            echo https://github.com/gccxml/gccxml/archive/master.zip
            ;;
        *)
            echo https://github.com/gccxml/gccxml/archive/master.zip
            ;;
    esac

}
function nexo-ext-libs-gccxml-download-filename {
    local version=${1:-$(nexo-ext-libs-gccxml-version)}
    case $version in
        master)
            echo gccxml-master.zip
            ;;
        *)
            echo gccxml-master.zip
            ;;
    esac

}

# build info
function nexo-ext-libs-gccxml-tardst {
    local version=${1:-$(nexo-ext-libs-gccxml-version)}
    case $version in
        master)
            echo gccxml-master
            ;;
        *)
            echo gccxml-master
            ;;
    esac

}

function nexo-ext-libs-gccxml-file-check-exist {
    nexo-ext-libs-file-check-exist $@
    return $?
}

# interface 
function nexo-ext-libs-gccxml-get {
    nexo-ext-libs-PKG-get gccxml
}

function nexo-ext-libs-gccxml-conf- {
    local msg="===== $FUNCNAME: "
    # begin to configure
    if [ ! -d "gccxml-build" ]; then
        mkdir gccxml-build
    fi
    pushd gccxml-build
    cmake .. -DCMAKE_INSTALL_PREFIX:PATH=$(nexo-ext-libs-gccxml-install-dir)
    popd
}

function nexo-ext-libs-gccxml-conf {
    nexo-ext-libs-PKG-conf gccxml
}

function nexo-ext-libs-gccxml-make- {
    local msg="===== $FUNCNAME: "
    pushd gccxml-build
    echo $msg make $(nexo-ext-libs-gen-cpu-num)
    make $(nexo-ext-libs-gen-cpu-num)
    popd
}
function nexo-ext-libs-gccxml-make {
    nexo-ext-libs-PKG-make gccxml
}

function nexo-ext-libs-gccxml-install- {
    local msg="===== $FUNCNAME: "
    pushd gccxml-build
    echo $msg make install
    make install
    popd
}
function nexo-ext-libs-gccxml-install {
    nexo-ext-libs-PKG-install gccxml
}

function nexo-ext-libs-gccxml-setup {
    nexo-ext-libs-PKG-setup gccxml
}
