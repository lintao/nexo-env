#!/bin/bash

# meta data
function nexo-ext-libs-python-virtualenv-name {
    echo python-virtualenv
}

function nexo-ext-libs-python-virtualenv-version-default {
    echo 1.11.6
}

function nexo-ext-libs-python-virtualenv-version {
    echo $(nexo-ext-libs-PKG-version python-virtualenv)
}

# dependencies
function nexo-ext-libs-python-virtualenv-dependencies-list {
    echo python
}
function nexo-ext-libs-python-virtualenv-dependencies-setup {
    nexo-ext-libs-dependencies-setup python-virtualenv
}

# install dir
# the installation prefix is the python's default path.
# install dir
function nexo-ext-libs-python-virtualenv-install-dir {
    local version=${1:-$(nexo-ext-libs-python-virtualenv-version)}
    echo $(nexo-ext-libs-install-root)/$(nexo-ext-libs-python-virtualenv-name)/$version
}

# download info
function nexo-ext-libs-python-virtualenv-download-url {
    local version=${1:-$(nexo-ext-libs-python-virtualenv-version)}
    case $version in
        1.11.6)
            echo https://pypi.python.org/packages/source/v/virtualenv/virtualenv-1.11.6.tar.gz
            ;;
        *) 
            echo https://pypi.python.org/packages/source/v/virtualenv/virtualenv-1.11.6.tar.gz
            ;;
    esac
}
function nexo-ext-libs-python-virtualenv-download-filename {
    local version=${1:-$(nexo-ext-libs-python-virtualenv-version)}
    case $version in
        1.11.6)
            echo virtualenv-1.11.6.tar.gz
            ;;
        *) 
            echo virtualenv-1.11.6.tar.gz
            ;;
    esac
}

function nexo-ext-libs-python-virtualenv-tardst {
    local version=${1:-$(nexo-ext-libs-python-virtualenv-version)}
    case $version in
        1.11.6)
            echo virtualenv-1.11.6
            ;;
        *) 
            echo virtualenv-1.11.6
            ;;
    esac
}

function nexo-ext-libs-python-virtualenv-file-check-exist {
    nexo-ext-libs-file-check-exist $@
    return $?
}

# get 
function nexo-ext-libs-python-virtualenv-get {
    nexo-ext-libs-PKG-get python-virtualenv
}

# conf

function nexo-ext-libs-python-virtualenv-conf-uncompress {
    local msg="===== $FUNCNAME: "
    tardst=$1
    tarname=$2
    echo $msg tar zxvf $tarname
    tar zxvf $tarname || exit $?
    if [ ! -d "$tardst" ]; then
        echo $msg "After Uncompress, can't find $tardst"
        exit 1
    fi
}
function nexo-ext-libs-python-virtualenv-conf- {
    local msg="===== $FUNCNAME: "

}
function nexo-ext-libs-python-virtualenv-conf {
    nexo-ext-libs-PKG-conf python-virtualenv
}

# make

function nexo-ext-libs-python-virtualenv-make- {
    local msg="===== $FUNCNAME: "
    python setup.py build
}
function nexo-ext-libs-python-virtualenv-make {
    nexo-ext-libs-PKG-make python-virtualenv
}

# install

function nexo-ext-libs-python-virtualenv-install- {
    local msg="===== $FUNCNAME: "
    python setup.py install
}
function nexo-ext-libs-python-virtualenv-install {
    nexo-ext-libs-PKG-install python-virtualenv
}

# setup


function nexo-ext-libs-python-virtualenv-setup {
    local msg="===== $FUNCNAME: "
}
