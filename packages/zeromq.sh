#!/bin/bash

# meta data
function nexo-ext-libs-zeromq-name {
    echo zeromq
}

function nexo-ext-libs-zeromq-version-default {
    echo 4.0.4
}

function nexo-ext-libs-zeromq-version {
    echo $(nexo-ext-libs-PKG-version zeromq)
}

# dependencies
function nexo-ext-libs-zeromq-dependencies-list {
    echo
}
function nexo-ext-libs-zeromq-dependencies-setup {
    nexo-ext-libs-dependencies-setup zeromq
}

# install dir
function nexo-ext-libs-zeromq-install-dir {
    local version=${1:-$(nexo-ext-libs-zeromq-version)}
    echo $(nexo-ext-libs-install-root)/$(nexo-ext-libs-zeromq-name)/$version
}

# download info
function nexo-ext-libs-zeromq-download-url {
    local version=${1:-$(nexo-ext-libs-zeromq-version)}
    case $version in
        4.0.4)
            echo http://download.zeromq.org/zeromq-4.0.4.tar.gz
            ;;
        *) 
            echo http://download.zeromq.org/zeromq-4.0.4.tar.gz
            ;;
    esac
}

function nexo-ext-libs-zeromq-download-filename {
    local version=${1:-$(nexo-ext-libs-zeromq-version)}
    case $version in
        4.0.4)
            echo zeromq-4.0.4.tar.gz
            ;;
        *) 
            echo zeromq-4.0.4.tar.gz
            ;;
    esac
}

function nexo-ext-libs-zeromq-tardst {
    local version=${1:-$(nexo-ext-libs-zeromq-version)}
    case $version in
        4.0.4)
            echo zeromq-4.0.4
            ;;
        *) 
            echo zeromq-4.0.4
            ;;
    esac
}

function nexo-ext-libs-zeromq-file-check-exist {
    nexo-ext-libs-file-check-exist $@
    return $?
}

# get 
function nexo-ext-libs-zeromq-get {
    nexo-ext-libs-PKG-get zeromq
}

# conf

function nexo-ext-libs-zeromq-conf- {
    local msg="===== $FUNCNAME: "

    echo $msg ./configure --prefix=$(nexo-ext-libs-zeromq-install-dir)
    ./configure --prefix=$(nexo-ext-libs-zeromq-install-dir) || exit $?

}
function nexo-ext-libs-zeromq-conf {
    nexo-ext-libs-PKG-conf zeromq
}

# make 
function nexo-ext-libs-zeromq-make {
    nexo-ext-libs-PKG-make zeromq
}

# install
function nexo-ext-libs-zeromq-install {
    nexo-ext-libs-PKG-install zeromq
}

# setup


function nexo-ext-libs-zeromq-setup {
    nexo-ext-libs-PKG-setup zeromq
}
