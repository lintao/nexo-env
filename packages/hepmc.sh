#!/bin/bash

# meta data
function nexo-ext-libs-hepmc-name {
    echo HepMC
}

function nexo-ext-libs-hepmc-version-default {
    echo 2.06.09
}
function nexo-ext-libs-hepmc-version {
    echo $(nexo-ext-libs-PKG-version hepmc)
}

# dependencies
function nexo-ext-libs-hepmc-dependencies-list {
    echo 
}
function nexo-ext-libs-hepmc-dependencies-setup {
    nexo-ext-libs-dependencies-setup hepmc
}
# install dir
function nexo-ext-libs-hepmc-install-dir {
    local version=${1:-$(nexo-ext-libs-hepmc-version)}
    echo $(nexo-ext-libs-install-root)/$(nexo-ext-libs-hepmc-name)/$version
}

# download info
function nexo-ext-libs-hepmc-download-url {
    local version=${1:-$(nexo-ext-libs-hepmc-version)}
    case $version in
        2.06.09)
            echo http://lcgapp.cern.ch/project/simu/HepMC/download/HepMC-2.06.09.tar.gz
            ;;
        *)
            echo http://lcgapp.cern.ch/project/simu/HepMC/download/HepMC-2.06.09.tar.gz
            ;;
    esac
}
function nexo-ext-libs-hepmc-download-filename {
    local version=${1:-$(nexo-ext-libs-hepmc-version)}
    case $version in
        2.06.09)
            echo HepMC-2.06.09.tar.gz
            ;;
        *)
            echo HepMC-2.06.09.tar.gz
            ;;
    esac
}
function nexo-ext-libs-hepmc-tardst {
    # install in root of external libraries
    local version=${1:-$(nexo-ext-libs-hepmc-version)}
    case $version in
        2.06.09)
            echo HepMC-2.06.09
            ;;
        *)
            echo HepMC-2.06.09
            ;;
    esac
}

function nexo-ext-libs-hepmc-file-check-exist {
    nexo-ext-libs-file-check-exist $@
    return $?
}

# interface
function nexo-ext-libs-hepmc-get {
    nexo-ext-libs-PKG-get hepmc
}
function nexo-ext-libs-hepmc-conf- {
    local msg="===== $FUNCNAME: "
    echo $msg ./configure --prefix=$(nexo-ext-libs-hepmc-install-dir) --with-momentum=MEV --with-length=MM
    ./configure --prefix=$(nexo-ext-libs-hepmc-install-dir) --with-momentum=MEV --with-length=MM
}
function nexo-ext-libs-hepmc-conf {
    nexo-ext-libs-PKG-conf hepmc
}

function nexo-ext-libs-hepmc-make {
    nexo-ext-libs-PKG-make hepmc
}

function nexo-ext-libs-hepmc-install {
    nexo-ext-libs-PKG-install hepmc
    # check the lib directory
    pushd $(nexo-ext-libs-hepmc-install-dir)
    if [ -d "lib64" -a ! -d "lib" ]; then
        ln -s lib64 lib
    fi
    popd
}
function nexo-ext-libs-hepmc-setup {
    nexo-ext-libs-PKG-setup hepmc
}
