#!/bin/bash

# meta data
function nexo-ext-libs-gsl-name {
    echo gsl
}

function nexo-ext-libs-gsl-version-default {
    echo 1.16
}

function nexo-ext-libs-gsl-version {
    echo $(nexo-ext-libs-PKG-version gsl)
}

# dependencies
function nexo-ext-libs-gsl-dependencies-list {
    echo
}
function nexo-ext-libs-gsl-dependencies-setup {
    nexo-ext-libs-dependencies-setup gsl
}

# install dir
function nexo-ext-libs-gsl-install-dir {
    local version=${1:-$(nexo-ext-libs-gsl-version)}
    echo $(nexo-ext-libs-install-root)/$(nexo-ext-libs-gsl-name)/$version
}

# download info
function nexo-ext-libs-gsl-download-url {
    local version=${1:-$(nexo-ext-libs-gsl-version)}
    case $version in
        1.16)
            echo ftp://ftp.gnu.org/gnu/gsl/gsl-1.16.tar.gz
            ;;
        *) 
            echo ftp://ftp.gnu.org/gnu/gsl/gsl-1.16.tar.gz
            ;;
    esac
}

function nexo-ext-libs-gsl-download-filename {
    local version=${1:-$(nexo-ext-libs-gsl-version)}
    case $version in
        1.16)
            echo gsl-1.16.tar.gz
            ;;
        *) 
            echo gsl-1.16.tar.gz
            ;;
    esac
}

function nexo-ext-libs-gsl-tardst {
    local version=${1:-$(nexo-ext-libs-gsl-version)}
    case $version in
        1.16)
            echo gsl-1.16
            ;;
        *) 
            echo gsl-1.16
            ;;
    esac
}

function nexo-ext-libs-gsl-file-check-exist {
    nexo-ext-libs-file-check-exist $@
    return $?
}

# get 
function nexo-ext-libs-gsl-get {
    nexo-ext-libs-PKG-get gsl
}

# conf

function nexo-ext-libs-gsl-conf- {
    local msg="===== $FUNCNAME: "

    echo $msg ./configure --prefix=$(nexo-ext-libs-gsl-install-dir)
    ./configure --prefix=$(nexo-ext-libs-gsl-install-dir) || exit $?

}
function nexo-ext-libs-gsl-conf {
    nexo-ext-libs-PKG-conf gsl
}

# make 
function nexo-ext-libs-gsl-make {
    nexo-ext-libs-PKG-make gsl
}

# install
function nexo-ext-libs-gsl-install {
    nexo-ext-libs-PKG-install gsl
}

# setup


function nexo-ext-libs-gsl-setup {
    nexo-ext-libs-PKG-setup gsl
}

