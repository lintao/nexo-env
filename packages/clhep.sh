#!/bin/bash

# meta data
function nexo-ext-libs-clhep-name {
    echo CLHEP
}

function nexo-ext-libs-clhep-version-default {
    echo 2.1.0.1
}
function nexo-ext-libs-clhep-version {
    echo $(nexo-ext-libs-PKG-version clhep)
}

# dependencies
function nexo-ext-libs-clhep-dependencies-list {
    echo 
}
function nexo-ext-libs-clhep-dependencies-setup {
    nexo-ext-libs-dependencies-setup clhep
}
# install dir
function nexo-ext-libs-clhep-install-dir {
    local version=${1:-$(nexo-ext-libs-clhep-version)}
    echo $(nexo-ext-libs-install-root)/$(nexo-ext-libs-clhep-name)/$version
}
# download info
function nexo-ext-libs-clhep-download-url {
    local version=${1:-$(nexo-ext-libs-clhep-version)}
    case $version in
        2.1.0.1)
            echo http://proj-clhep.web.cern.ch/proj-clhep/DISTRIBUTION/tarFiles/clhep-2.1.0.1.tgz
            ;;
        *)
            echo http://proj-clhep.web.cern.ch/proj-clhep/DISTRIBUTION/tarFiles/clhep-2.1.0.1.tgz
            ;;
    esac
}
function nexo-ext-libs-clhep-download-filename {
    local version=${1:-$(nexo-ext-libs-clhep-version)}
    case $version in
        2.1.0.1)
            echo clhep-2.1.0.1.tgz
            ;;
        *)
            echo clhep-2.1.0.1.tgz
            ;;
    esac
}
function nexo-ext-libs-clhep-tardst {
    # install in root of external libraries
    local version=${1:-$(nexo-ext-libs-clhep-version)}
    case $version in
        2.1.0.1)
            echo clhep-2.1.0.1
            ;;
        *)
            echo clhep-2.1.0.1
            ;;
    esac
}

function nexo-ext-libs-clhep-file-check-exist {
    nexo-ext-libs-file-check-exist $@
    return $?
}

# interface
function nexo-ext-libs-clhep-get {
    nexo-ext-libs-PKG-get clhep
}

function nexo-ext-libs-clhep-conf-uncompress {
    local msg="===== $FUNCNAME: "
    tardst=$1
    tarname=$2
    echo tar -C $tardst -zxvf $tarname
    if [ -d "$tardst" ]; then
        echo $msg $tardst already exist
        echo $msg will not invoke the uncompress command
        return
    else
        mkdir -p $tardst
    fi
    tar -C $tardst -zxvf $tarname || exit $?
    if [ ! -d "$tardst" ]; then
        echo $msg "After Uncompress, can't find $tardst"
        exit 1
    fi
}

function nexo-ext-libs-clhep-conf- {
    local msg="===== $FUNCNAME: "
    pushd $(nexo-ext-libs-clhep-version)/CLHEP
    echo $msg ./configure --prefix=$(nexo-ext-libs-clhep-install-dir) 
    ./configure --prefix=$(nexo-ext-libs-clhep-install-dir) 
    popd
}
function nexo-ext-libs-clhep-conf {
    nexo-ext-libs-PKG-conf clhep
}
function nexo-ext-libs-clhep-make- {
    local msg="===== $FUNCNAME: "
    pushd $(nexo-ext-libs-clhep-version)/CLHEP
    echo $msg make $(nexo-ext-libs-gen-cpu-num)
    make $(nexo-ext-libs-gen-cpu-num)
    popd
}
function nexo-ext-libs-clhep-make {
    nexo-ext-libs-PKG-make clhep
}
function nexo-ext-libs-clhep-install- {
    local msg="===== $FUNCNAME: "
    pushd $(nexo-ext-libs-clhep-version)/CLHEP
    echo $msg make install
    make install
    popd
    # check the lib directory
    pushd $(nexo-ext-libs-clhep-install-dir)
    if [ -d "lib64" -a ! -d "lib" ]; then
        ln -s lib64 lib
    fi
    popd
}

function nexo-ext-libs-clhep-install {
    nexo-ext-libs-PKG-install clhep
}
function nexo-ext-libs-clhep-setup {
    nexo-ext-libs-PKG-setup clhep
}
