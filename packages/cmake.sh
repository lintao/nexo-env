#!/bin/bash

# meta data
function nexo-ext-libs-cmake-name {
    echo Cmake
}

function nexo-ext-libs-cmake-version-default {
    echo 2.8.12.1
}
function nexo-ext-libs-cmake-version {
    echo $(nexo-ext-libs-PKG-version cmake)
}

# dependencies
function nexo-ext-libs-cmake-dependencies-list {
    echo
}
function nexo-ext-libs-cmake-dependencies-setup {
    nexo-ext-libs-dependencies-setup cmake
}

# install dir
function nexo-ext-libs-cmake-install-dir {
    local version=${1:-$(nexo-ext-libs-cmake-version)}
    echo $(nexo-ext-libs-install-root)/$(nexo-ext-libs-cmake-name)/$version
}

# download info
function nexo-ext-libs-cmake-download-url {
    local version=${1:-$(nexo-ext-libs-cmake-version)}
    case $version in
        2.8.12.1)
            echo http://www.cmake.org/files/v2.8/cmake-2.8.12.1.tar.gz
            ;;
        *) 
            echo http://www.cmake.org/files/v2.8/cmake-2.8.12.1.tar.gz
            ;;
    esac
}

function nexo-ext-libs-cmake-download-filename {
    local version=${1:-$(nexo-ext-libs-cmake-version)}
    case $version in
        2.8.12.1)
            echo cmake-2.8.12.1.tar.gz
            ;;
            *)
            echo cmake-2.8.12.1.tar.gz
            ;;
    esac

}

# build info
function nexo-ext-libs-cmake-tardst {
    local version=${1:-$(nexo-ext-libs-cmake-version)}
    case $version in
        2.8.12.1)
            echo cmake-2.8.12.1
            ;;
            *)
            echo cmake-2.8.12.1
            ;;
    esac

}

function nexo-ext-libs-cmake-file-check-exist {
    nexo-ext-libs-file-check-exist $@
    return $?
}

# interface

function nexo-ext-libs-cmake-get {
    nexo-ext-libs-PKG-get cmake
}

function nexo-ext-libs-cmake-conf- {
    local msg="===== $FUNCNAME: "
    # begin to configure
    echo $msg ./bootstrap --prefix=$(nexo-ext-libs-cmake-install-dir)
    ./bootstrap --prefix=$(nexo-ext-libs-cmake-install-dir)
}
function nexo-ext-libs-cmake-conf {
    nexo-ext-libs-PKG-conf cmake
}

function nexo-ext-libs-cmake-make {
    nexo-ext-libs-PKG-make cmake
}

function nexo-ext-libs-cmake-install {
    nexo-ext-libs-PKG-install cmake
}

function nexo-ext-libs-cmake-setup {
    nexo-ext-libs-PKG-setup cmake
}
