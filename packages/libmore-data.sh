#!/bin/bash
# meta

function nexo-ext-libs-libmore-data-name {
    echo libmore-data
}

function nexo-ext-libs-libmore-data-version-default {
    echo 20140630
}

function nexo-ext-libs-libmore-data-version {
    echo $(nexo-ext-libs-PKG-version libmore-data)
}

# dependencies
function nexo-ext-libs-libmore-data-dependencies-list {
    echo libmore
}
function nexo-ext-libs-libmore-data-dependencies-setup {
    nexo-ext-libs-dependencies-setup libmore-data
}

function nexo-ext-libs-libmore-data-install-dir {
    echo $(nexo-ext-libs-libmore-install-dir $*)/com/more/ensdf
}

# download info
function nexo-ext-libs-libmore-data-download-url {
    local version=${1:-$(nexo-ext-libs-libmore-version)}
    case $version in
        20140630) 
            echo http://nexo.ihep.ac.cn/software/offline/tarFiles/ensdf-files-20140630.tar
            ;;
        *) 
            echo http://nexo.ihep.ac.cn/software/offline/tarFiles/ensdf-files-20140630.tar
            ;;
    esac
}

function nexo-ext-libs-libmore-data-download-filename {
    local version=${1:-$(nexo-ext-libs-libmore-version)}
    case $version in
        20140630)
            echo ensdf-files-20140630.tar
            ;;
        *) 
            echo ensdf-files-20140630.tar
            ;;
    esac
}


function nexo-ext-libs-libmore-data-tardst {
    local version=${1:-$(nexo-ext-libs-libmore-version)}
    case $version in
        0.8.3)
            echo ensdf
            ;;
        *) 
            echo ensdf
            ;;
    esac
}

function nexo-ext-libs-libmore-data-file-check-exist {
    nexo-ext-libs-file-check-exist $@
    return $?
}

# get

function nexo-ext-libs-libmore-data-get {
    nexo-ext-libs-PKG-get libmore-data
}

# conf

function nexo-ext-libs-libmore-data-conf-uncompress {
    local msg="===== $FUNCNAME: "
    tardst=$1
    tarname=$2
    echo $msg tar xvf $tarname
    tar xvf $tarname || exit $?
    if [ ! -d "$tardst" ]; then
        echo $msg "After Uncompress, can't find $tardst"
        exit 1
    fi
}

function nexo-ext-libs-libmore-data-conf- {
    local msg="===== $FUNCNAME: "
}
function nexo-ext-libs-libmore-data-conf {
    nexo-ext-libs-PKG-conf libmore-data
}
# make 
function nexo-ext-libs-libmore-data-make {
    local msg="===== $FUNCNAME: "
}

# install
function nexo-ext-libs-libmore-data-install- {
    local msg="===== $FUNCNAME: "

    # if we are in the $tardst, copy files into install area
    rsync -avz . $(nexo-ext-libs-libmore-data-install-dir)
}

function nexo-ext-libs-libmore-data-install {
    nexo-ext-libs-PKG-install libmore-data
}

# setup


function nexo-ext-libs-libmore-data-setup {
    local msg="===== $FUNCNAME: "
}
