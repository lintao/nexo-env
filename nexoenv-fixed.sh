# = helper =
function nexoenv-fixed-dst-detect () {
    local dst=${1:-}
    if [ -n "$dst" ]; then
        echo $dst
        return
    fi
    # assume the parent directory of nexoenv is the NEXOTOP
    local dirnexoenv=$NEXOENVDIR
    local dirnexotop=$(dirname $NEXOENVDIR)
    echo $dirnexotop
}

function nexoenv-fixed-src-detect () {
    local msg="==== $FUNCNAME: "
    local src=${1:-}
    if [ -n "$src" ]; then
        echo $src
        return
    fi
    # assume the parent directory of nexoenv is the NEXOTOP
    local dirnexoenv=$NEXOENVDIR
    local dirnexotop=$(dirname $NEXOENVDIR)
    # get the NEXOTOP from the old setup script
    local bashrcsh=$dirnexotop/bashrc.sh
    if [ -f "$bashrcsh" ]; then
        local result=$(grep NEXOTOP $bashrcsh)
        if [ "$?" == 0 -a -n "$result" ] ; then
            echo $result | cut -d '=' -f 2
            return 0
        fi
    fi
    echo $msg Can not detect the original NEXOTOP 1>&2
    exit 1
}

function nexoenv-fixed-check-dst-src () {
    local msg="==== $FUNCNAME: "
    local dst=$1
    local src=$2
    # if the src and dst are same, don't do any thing.
    if [ "$src" == "$dst" ]; then
        return 1
    fi
    return 0
}

# = Replace the directories =
function nexoenv-fiexed-shell-script-replace () {
    local script=$1
    local dst=$2
    local src=$3
    sed -i 's;'$src';'$dst';' $script
}

function nexoenv-fiexed-list-all-script() {
    # find the common script first, .sh or .csh
    find . -name \*sh
    find . -name bashrc
    find . -name tcshrc
    # fix root
    find . -name \*rc -type f
    find . -name root-config
    find . -name \*.h
}

function nexoenv-fixed-replace-all() {
    local dst=$1
    local src=$2
    pushd $(nexo-top-dir)
    local script=""
    for script in $(nexoenv-fiexed-list-all-script)
    do
        nexoenv-fiexed-shell-script-replace $script $dst $src
    done
    popd
}

# = main =
function nexoenv-fixed () {
    local msg="=== $FUNCNAME: "
    # chang the directory name in setup script.
    local dst=$(nexoenv-fixed-dst-detect $1)
    local src=$(nexoenv-fixed-src-detect $2)

    echo $msg SRC: $src 1>&2
    echo $msg DST: $dst 1>&2
    nexoenv-fixed-check-dst-src $dst $src
    local st=$?
    if [ "$st" == "1" ]; then
        echo $msg src and dst are the same one, skip fixed. 1>&2
        return 0
    fi

    nexoenv-fixed-replace-all $dst $src
}
