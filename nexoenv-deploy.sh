# = deploy the different modules =
# == general helper to deploy the compressed data ==
function nexoenv-deploy-XXX {
    local msg="==== $FUNCNAME: "
    local mod=$1
    local mod_tarname=""
    local mod_dirname=""
    # - check -
    # -- check tar name --
    if type -t nexoenv-archive-$mod-tarname >& /dev/null; then
        mod_tarname=$(nexoenv-archive-$mod-tarname)
    else
        echo $msg nexoenv-archive-$mod-tarname not exist 1>&2
        exit 1
    fi
    # -- check dir name --
    if type -t nexoenv-archive-$mod-dirname >& /dev/null; then
        mod_dirname=$(nexoenv-archive-$mod-dirname)
    else
        echo $msg nexoenv-archive-$mod-tarname not exist 1>&2
        exit 1
    fi
    # - begin -
    pushd $(nexo-top-dir) >& /dev/null
    # -- check the existence of the tarballs --
    if [ ! -f "$mod_tarname" ]; then
        echo $msg $mod_tarname does not exist 1>&2
        exit 1
    fi
    # -- TODO checksum --
    # -- check the directory or file already exists or not --
    local m="" # the $mod_dirname may be a list of files or dirs
    local exists=true
    for m in $mod_dirname
    do
        if [ -e "$m" ]; then
            continue;
        else
            exists=false;
        fi
    done
    if  $exists = true ; then
        echo $msg $mod_dirname already exists, skip deploy 1>&2
    else 
        # -- finally, deploy the tarball --
        echo $msg begin: deploy $mod_dirname 1>&2
        tar zxvf $mod_tarname
        echo $msg end:   deploy $mod_dirname 1>&2
    fi
    popd >& /dev/null
}
# == libs ==
function nexoenv-deploy-libs {
    local msg="=== $FUNCNAME: "
    nexoenv-deploy-XXX libs
}
# == libs build ==
function nexoenv-deploy-libsbuild {
    local msg="=== $FUNCNAME: "
    nexoenv-deploy-XXX libsbuild
}
# == cmtlibs ==
function nexoenv-deploy-cmtlibs {
    local msg="=== $FUNCNAME: "
    nexoenv-deploy-XXX cmtlibs
}
# == sniper ==
function nexoenv-deploy-sniper {
    local msg="=== $FUNCNAME: "
    nexoenv-deploy-XXX sniper
}
# == offline ==
function nexoenv-deploy-offline {
    local msg="=== $FUNCNAME: "
    nexoenv-deploy-XXX offline
}
# == setup ==
function nexoenv-deploy-setup {
    local msg="=== $FUNCNAME: "
    nexoenv-deploy-XXX setup
}
# == all ==
function nexoenv-deploy-all {
    local msg="=== $FUNCNAME: "
    local mods="libs cmtlibs sniper offline setup"
    local module=""

    for module in $mods
    do
        nexoenv-deploy-$module
    done
}

# = main =
function nexoenv-deploy {
    local msg="== $FUNCNAME: "
    local mods=$@
    if [ -z "$mods" ];
    then
        mods=all
    fi
    local module=""
    for module in $mods 
    do
        case $module in
            all)
                nexoenv-deploy-all
                ;;
            setup)
                nexoenv-deploy-setup
                ;;
            libs|cmtlibs|libsbuild)
                nexoenv-deploy-$module
                ;;
            sniper)
                nexoenv-deploy-sniper
                ;;
            offline)
                nexoenv-deploy-offline
                ;;
        esac
    done
}
