#!/bin/bash
function nexoenv-offline-name {
    echo offline
}

function nexoenv-offline-version {
    local msg="==== $FUNCNAME"
    version=$1
    nexo-svn-check-branches-name $version
    return $?
}

function nexoenv-offline-url {
    local msg="==== $FUNCNAME: "
    local version=${1:-trunk}
    local branchesname=$(nexoenv-offline-version $version)
    svnurl=$(nexo-svn-top)/$(nexoenv-offline-name)/${branchesname}
    nexo-svn-check-repo-url $svnurl
    return $?
}

function nexoenv-offline-revision-gen {
    nexo-svn-revision-gen $*
}

function nexoenv-offline-checkout {
    # checkout all code
    local msg="==== $FUNCNAME: "
    local svnurl=$1
    local dstname=$2
    local revision=$3
    echo $msg checkout the code $svnurl 
    # check
    local flag=$(nexoenv-offline-revision-gen $revision)
    echo $msg svn co $flag $svnurl $dstname 
    svn co $flag $svnurl $dstname 
}

function nexoenv-offline-preq {
    source $NEXOENVDIR/nexoenv-env.sh
    local setupscript=$(nexo-top-dir)/setup.sh
    if [ -f "$setupscript" ]; then
        pushd $(nexo-top-dir) >& /dev/null
        source $setupscript
        popd
    else
        echo $msg Please using "nexoenv env" to setup the environment first 1>&2
        exit 1
    fi
}

function nexoenv-offline-compile {
    local msg="==== $FUNCNAME: "
    pushd $(nexo-top-dir) >& /dev/null
    if [ -d "$(nexoenv-offline-name)" ]; then
        pushd $(nexoenv-offline-name)/JunoRelease/cmt >& /dev/null
        cmt br cmt config
        source setup.sh
        cmt br cmt make
        popd
    fi

    popd >& /dev/null

}

function nexoenv-offline {
    local msg="=== $FUNCNAME: "
    # the main handler in this script
    local branchname=${1:-trunk}
    local revision=${2:-}
    # check version
    nexoenv-offline-version $branchname
    if [ "$?" != "0" ]; then
        echo $msg branchesname ret: $?
        return 1
    fi


    local url=$(nexoenv-offline-url $branchname) 
    echo $msg $?
    echo $msg URL: $url
    # change directory to $NEXOTOP
    pushd $NEXOTOP >& /dev/null
    nexoenv-offline-checkout $url $(nexoenv-offline-name) $revision
    nexoenv-offline-preq
    nexoenv-offline-compile
    popd

}
