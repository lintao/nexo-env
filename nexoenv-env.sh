#!/bin/bash

function nexoenv-env-bashrc-name {
    echo bashrc.sh
}
function nexoenv-env-tcshrc-name {
    echo tcshrc.csh
}

function nexoenv-env-setup-sh-name {
    echo setup.sh
}

function nexoenv-env-setup-csh-name {
    echo setup.csh
}

function nexoenv-env-script-check {
    local msg="=== $FUNCNAME: "
    local retcode=0
    pushd $(nexo-top-dir) >& /dev/null
    for name in $(nexoenv-env-bashrc-name) $(nexoenv-env-tcshrc-name)
    do
        if [ -f "$name" ]; then
            echo $msg $name already exists in $(nexo-top-dir)
            retcode=1
        fi
    done
    popd >& /dev/null
    return $retcode
}

function nexoenv-env-setup-check {
    local msg="=== $FUNCNAME: "
    nexoenv-env-script-check
    if [ "$?" = "0" ];then
        echo $msg Checking OK
    else
        echo $msg The setup scripts seems already exists.
        echo $msg If you want to resetup, please use 
        echo $msg ==== $ nexoenv env resetup
        echo $msg first, it will rename your setup scripts. Then use
        echo $msg ==== $ nexoenv env
        echo $msg to resetup.
        exit 1
    fi
}
# NEXOTOP
function nexoenv-env-setup-nexotop {
    local msg="=== $FUNCNAME: "
    echo $msg
    pushd $(nexo-top-dir)>& /dev/null

    echo $msg Create $(nexoenv-env-bashrc-name)
cat << SHCMTPP >> $(nexoenv-env-bashrc-name)
export NEXOTOP=$(nexo-top-dir)
SHCMTPP

    echo $msg Create $(nexoenv-env-tcshrc-name)
cat << CSHCMTPP >> $(nexoenv-env-tcshrc-name)
setenv NEXOTOP $(nexo-top-dir)
CSHCMTPP
    popd >& /dev/null


}
# CMTPROJECTPATH
function nexoenv-env-setup-cmtprojectpath {
    local msg="=== $FUNCNAME: "
    pushd $(nexo-top-dir)>& /dev/null

    echo $msg Append $(nexoenv-env-bashrc-name)
cat << SHCMTPP >> $(nexoenv-env-bashrc-name)
export CMTPROJECTPATH=$(nexo-top-dir):\${CMTPROJECTPATH}
SHCMTPP

    echo $msg Append $(nexoenv-env-tcshrc-name)
cat << CSHCMTPP >> $(nexoenv-env-tcshrc-name)
if ( \$?CMTPROJECTPATH == 0 ) then
    setenv CMTPROJECTPATH ""
endif
setenv CMTPROJECTPATH $(nexo-top-dir):\${CMTPROJECTPATH}
CSHCMTPP
    popd >& /dev/null

}
# External Libraries
# TODO
function nexoenv-env-setup-external-libraries-list {
    # Here is a little complicated.
    # The output should be clean
    local guesspkg=""
    local env_scripts_dir=$NEXOENVDIR/packages
    source $NEXOENVDIR/nexoenv-external-libs.sh
    for guesspkg in $(ls $env_scripts_dir/*.sh)
    do
        source $guesspkg
        local pkg_short_name=$(basename $guesspkg)
        pkg_short_name="${pkg_short_name%.*}"

        # check the bashrc and tcshrc in the External Libraries
        local installdir=$(nexo-ext-libs-${pkg_short_name}-install-dir)
        if [ -f "$installdir/bashrc" -a -f "$installdir/tcshrc" ]; then
            echo $installdir
        fi
    done
}
function nexoenv-env-setup-external-libraries {
    local msg="=== $FUNCNAME: "
    local extlibinnexo=""
    pushd $(nexo-top-dir)>& /dev/null
    for  extlibinnexo in $(nexoenv-env-setup-external-libraries-list)
    do

    echo $msg Append $(nexoenv-env-bashrc-name): ${extlibinnexo}
cat << SHCMTPP >> $(nexoenv-env-bashrc-name)
source ${extlibinnexo}/bashrc
SHCMTPP

    echo $msg Append $(nexoenv-env-tcshrc-name): ${extlibinnexo}
cat << CSHCMTPP >> $(nexoenv-env-tcshrc-name)
source ${extlibinnexo}/tcshrc
CSHCMTPP

    done

    popd >& /dev/null
}

# == whole env, include setup offline ==
function nexoenv-env-setup-runtime-offline-sh {
cat << SETUPSH > $(nexoenv-env-setup-sh-name)
export NEXOTOP=$(nexo-top-dir)
pushd \$NEXOTOP >& /dev/null
    source bashrc.sh

    pushd ExternalInterface/EIRelease/cmt/ >& /dev/null
    source setup.sh
    popd >& /dev/null

    pushd sniper/SniperRelease/cmt/ >& /dev/null
    source setup.sh
    popd >& /dev/null

    # setup offline
    # pushd offline/JunoRelease/cmt/ >& /dev/null
    # source setup.sh
    # popd >& /dev/null

    # setup tutorial 
    if [ -z "\${NEXO_OFFLINE_OFF:-}" ];
    then
        echo Setup Official Offline Software
        pushd offline/Examples/Tutorial/cmt/ >& /dev/null
        source setup.sh
        popd >& /dev/null
    fi
popd >& /dev/null

SETUPSH
}

function nexoenv-env-setup-runtime-offline-csh {
cat << SETUPCSH > $(nexoenv-env-setup-csh-name)
setenv NEXOTOP $(nexo-top-dir)
pushd \$NEXOTOP >& /dev/null
    source tcshrc.csh

    pushd ExternalInterface/EIRelease/cmt/ >& /dev/null
    source setup.csh
    popd >& /dev/null

    pushd sniper/SniperRelease/cmt/ >& /dev/null
    source setup.csh
    popd >& /dev/null

    # setup offline
    # pushd offline/JunoRelease/cmt/ >& /dev/null
    # source setup.csh
    # popd >& /dev/null

    # setup tutorial 
    if ( \$?NEXO_OFFLINE_OFF == 0 ) then
        echo Setup Official Offline Software
        pushd offline/Examples/Tutorial/cmt/ >& /dev/null
        source setup.csh
        popd >& /dev/null
    endif
popd >& /dev/null

SETUPCSH
}

function nexoenv-env-setup-runtime-offline {
    pushd $(nexo-top-dir) >& /dev/null
    nexoenv-env-setup-runtime-offline-sh
    nexoenv-env-setup-runtime-offline-csh
    popd >& /dev/null
}

# = sub command =
# == resetup ==
function nexoenv-env-resetup {
    local msg="=== $FUNCNAME: "
    # remove the existed:
    # * bashrc, tcshrc
    pushd $(nexo-top-dir) >& /dev/null
    for name in $(nexoenv-env-bashrc-name) $(nexoenv-env-tcshrc-name)
    do
        echo $msg remove $name 1>&2 
        rm $name 
    done
    popd >& /dev/null

    # * external related libraries
    nexoenv-env-setup-check 
    echo $msg setup the whole system
    nexoenv-env-setup-nexotop
    nexoenv-env-setup-cmtprojectpath
    nexoenv-env-setup-external-libraries
    # * whole setup
    nexoenv-env-setup-runtime-offline
}

# main interface

function nexoenv-env {
    local msg="== $FUNCNAME: "
    local subcommand=$1
    shift
    # If there is any sub command
    if [ -n "$subcommand" ]; then
        nexoenv-env-$subcommand
    else
        nexoenv-env-setup-check 
        echo $msg setup the whole system
        nexoenv-env-setup-nexotop
        nexoenv-env-setup-cmtprojectpath
        nexoenv-env-setup-external-libraries
        nexoenv-env-setup-runtime-offline
    fi
}
