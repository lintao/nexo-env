function nexoenv-archive-doc {
cat << DOC
The archive mode is used to create archives from our nexo offline software.

Supported modules to archive:
 * all (including libs, cmtlibs, sniper, offline), default module.
 * libs: Compiled external libraries
 * cmtlibs: External Interface
 * sniper: Sniper
 * offline: Offline Software
 * libsbuild: include compiled libraries and 
              the Source code and object files to build the external libraries.

After we already have the tar.gz file, we can deploy the environment again.
Ref: 
 * https://www.kernel.org/pub/software/scm/git/docs/git-archive.html
DOC
}
# = destination =
function nexoenv-archive-dest {
    echo $(nexo-top-dir)
}
# = archive different modules =
# == PKG named XXX, general function ==
function nexoenv-archive-XXX {
    local msg="==== $FUNCNAME: "
    local mod=$1
    local mod_tarname=""
    local mod_dirname=""
    local str_exclude=""
    # - check -
    # -- check tar name --
    if type -t nexoenv-archive-$mod-tarname >& /dev/null; then
        mod_tarname=$(nexoenv-archive-$mod-tarname)
    else
        echo $msg nexoenv-archive-$mod-tarname not exist 1>&2
        exit 1
    fi
    # -- check dir name --
    if type -t nexoenv-archive-$mod-dirname >& /dev/null; then
        mod_dirname=$(nexoenv-archive-$mod-dirname)
    else
        echo $msg nexoenv-archive-$mod-tarname not exist 1>&2
        exit 1
    fi
    # -- exclude dir --
    if type -t nexoenv-archive-$mod-exclude >& /dev/null; then
        local excludedir;
        for excludedir in $(nexoenv-archive-$mod-exclude)
        do
            str_exclude="$str_exclude --exclude=$excludedir"
        done
    fi
    # - begin -
    pushd $(nexo-top-dir) >& /dev/null
    if [ -f "$mod_tarname" ]; then
        echo $msg $mod_tarname already exists, skip create a new one. 1>&2
    else
        tar -zcvf $mod_tarname $mod_dirname $str_exclude
    fi
    # - checksum -
    local csmethod=""
    for csmethod in sha256 md5 
    do
        nexoenv-archive-checksum-$csmethod $mod_tarname
    done
    popd >& /dev/null
}
# === helper: calculate the md5 or sha256 ===
function nexoenv-archive-checksum-XXX {
    local msg="==== $FUNCNAME: "
    local method=${1:-md5}
    shift;
    local filename=${1:-}
    if [ -z "$filename" ]; then
        echo $msg Please input a filename to calculate $method 1>&2
        exit 1
    fi
    if [ ! -f "$filename" ]; then
        echo $msg Make sure $filename exists 1>&2
        exit 1
    fi
    local filewithchecksum=${filename}.${method}
    if [ -f $filewithchecksum ]; then
        echo $msg $filewithchecksum already exists, skip calculate 1>&2
        return 0
    fi
    local checksumexe=""
    if type -t nexoenv-archive-checksum-$method-exe >& /dev/null; then
        checksumexe=$(nexoenv-archive-checksum-$method-exe)
    else
        echo $msg nexoenv-archive-checksum-$method-exe not exist 1>&2
        exit 1
    fi

    $checksumexe $filename > $filewithchecksum
}
# ==== sha256 ====
function nexoenv-archive-checksum-sha256-exe {
    echo sha256sum
}
function nexoenv-archive-checksum-sha256 {
    local filename=${1:-}
    nexoenv-archive-checksum-XXX sha256 $filename
}
# ==== md5 ====
function nexoenv-archive-checksum-md5-exe {
    echo md5sum
}
function nexoenv-archive-checksum-md5 {
    local filename=${1:-}
    nexoenv-archive-checksum-XXX md5 $filename
}
# == libs ==
function nexoenv-archive-libs-tarname {
    echo libs.tar.gz
}
function nexoenv-archive-libs-dirname {
    echo ExternalLibs
}
function nexoenv-archive-libs-exclude {
    echo ExternalLibs/Build
}
function nexoenv-archive-libs {
    local msg="==== $FUNCNAME: "
    nexoenv-archive-XXX libs
}
# == libs build ==
function nexoenv-archive-libsbuild-tarname {
    echo libsbuild.tar.gz
}
function nexoenv-archive-libsbuild-dirname {
    echo ExternalLibs
}
function nexoenv-archive-libsbuild-exclude {
    echo 
}
function nexoenv-archive-libsbuild {
    local msg="==== $FUNCNAME: "
    nexoenv-archive-XXX libsbuild
}
# == cmtlibs ==
function nexoenv-archive-cmtlibs-tarname {
    echo cmtlibs.tar.gz
}
function nexoenv-archive-cmtlibs-dirname {
    echo ExternalInterface
}
function nexoenv-archive-cmtlibs-exclude {
    echo 
}
function nexoenv-archive-cmtlibs {
    local msg="==== $FUNCNAME: "
    nexoenv-archive-XXX cmtlibs
}
# == sniper ==
function nexoenv-archive-sniper-tarname {
    echo sniper.tar.gz
}
function nexoenv-archive-sniper-dirname {
    echo sniper
}
function nexoenv-archive-sniper-exclude {
    echo 
}
function nexoenv-archive-sniper {
    local msg="==== $FUNCNAME: "
    nexoenv-archive-XXX sniper
}
# == offline ==
function nexoenv-archive-offline-tarname {
    echo offline.tar.gz
}
function nexoenv-archive-offline-dirname {
    echo offline
}
function nexoenv-archive-offline-exclude {
    echo 
}
function nexoenv-archive-offline {
    local msg="==== $FUNCNAME: "
    nexoenv-archive-XXX offline
}
# == setup ==
function nexoenv-archive-setup-tarname {
    echo setup.tar.gz
}
function nexoenv-archive-setup-dirname {
    echo setup.sh setup.csh bashrc.sh tcshrc.csh
}
function nexoenv-archive-setup-exclude {
    echo 
}
function nexoenv-archive-setup {
    local msg="==== $FUNCNAME: "
    nexoenv-archive-XXX setup
}
# == all ==
function nexoenv-archive-all {
    local msg="=== $FUNCNAME: "
    local mods="libs cmtlibs sniper offline setup"
    local module=""

    for module in $mods
    do
        nexoenv-archive-$module
    done
}
# = main =
function nexoenv-archive {
    local msg="== $FUNCNAME: "
    local mods=$@
    if [ -z "$mods" ];
    then
        mods=all
    fi
    local module=""
    for module in $mods 
    do
        case $module in
            all)
                nexoenv-archive-all
                ;;
            setup)
                nexoenv-archive-setup
                ;;
            libs|cmtlibs|libsbuild)
                nexoenv-archive-$module
                ;;
            sniper)
                nexoenv-archive-sniper
                ;;
            offline)
                nexoenv-archive-offline
                ;;
        esac
    done
}
